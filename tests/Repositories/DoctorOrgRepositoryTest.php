<?php namespace Tests\Repositories;

use App\Models\DoctorOrg;
use App\Repositories\DoctorOrgRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DoctorOrgRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DoctorOrgRepository
     */
    protected $doctorOrgRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->doctorOrgRepo = \App::make(DoctorOrgRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_doctor_org()
    {
        $doctorOrg = DoctorOrg::factory()->make()->toArray();

        $createdDoctorOrg = $this->doctorOrgRepo->create($doctorOrg);

        $createdDoctorOrg = $createdDoctorOrg->toArray();
        $this->assertArrayHasKey('id', $createdDoctorOrg);
        $this->assertNotNull($createdDoctorOrg['id'], 'Created DoctorOrg must have id specified');
        $this->assertNotNull(DoctorOrg::find($createdDoctorOrg['id']), 'DoctorOrg with given id must be in DB');
        $this->assertModelData($doctorOrg, $createdDoctorOrg);
    }

    /**
     * @test read
     */
    public function test_read_doctor_org()
    {
        $doctorOrg = DoctorOrg::factory()->create();

        $dbDoctorOrg = $this->doctorOrgRepo->find($doctorOrg->id);

        $dbDoctorOrg = $dbDoctorOrg->toArray();
        $this->assertModelData($doctorOrg->toArray(), $dbDoctorOrg);
    }

    /**
     * @test update
     */
    public function test_update_doctor_org()
    {
        $doctorOrg = DoctorOrg::factory()->create();
        $fakeDoctorOrg = DoctorOrg::factory()->make()->toArray();

        $updatedDoctorOrg = $this->doctorOrgRepo->update($fakeDoctorOrg, $doctorOrg->id);

        $this->assertModelData($fakeDoctorOrg, $updatedDoctorOrg->toArray());
        $dbDoctorOrg = $this->doctorOrgRepo->find($doctorOrg->id);
        $this->assertModelData($fakeDoctorOrg, $dbDoctorOrg->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_doctor_org()
    {
        $doctorOrg = DoctorOrg::factory()->create();

        $resp = $this->doctorOrgRepo->delete($doctorOrg->id);

        $this->assertTrue($resp);
        $this->assertNull(DoctorOrg::find($doctorOrg->id), 'DoctorOrg should not exist in DB');
    }
}
