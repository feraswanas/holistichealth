<?php namespace Tests\Repositories;

use App\Models\OrgSchedule;
use App\Repositories\OrgScheduleRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class OrgScheduleRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var OrgScheduleRepository
     */
    protected $orgScheduleRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->orgScheduleRepo = \App::make(OrgScheduleRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_org_schedule()
    {
        $orgSchedule = OrgSchedule::factory()->make()->toArray();

        $createdOrgSchedule = $this->orgScheduleRepo->create($orgSchedule);

        $createdOrgSchedule = $createdOrgSchedule->toArray();
        $this->assertArrayHasKey('id', $createdOrgSchedule);
        $this->assertNotNull($createdOrgSchedule['id'], 'Created OrgSchedule must have id specified');
        $this->assertNotNull(OrgSchedule::find($createdOrgSchedule['id']), 'OrgSchedule with given id must be in DB');
        $this->assertModelData($orgSchedule, $createdOrgSchedule);
    }

    /**
     * @test read
     */
    public function test_read_org_schedule()
    {
        $orgSchedule = OrgSchedule::factory()->create();

        $dbOrgSchedule = $this->orgScheduleRepo->find($orgSchedule->id);

        $dbOrgSchedule = $dbOrgSchedule->toArray();
        $this->assertModelData($orgSchedule->toArray(), $dbOrgSchedule);
    }

    /**
     * @test update
     */
    public function test_update_org_schedule()
    {
        $orgSchedule = OrgSchedule::factory()->create();
        $fakeOrgSchedule = OrgSchedule::factory()->make()->toArray();

        $updatedOrgSchedule = $this->orgScheduleRepo->update($fakeOrgSchedule, $orgSchedule->id);

        $this->assertModelData($fakeOrgSchedule, $updatedOrgSchedule->toArray());
        $dbOrgSchedule = $this->orgScheduleRepo->find($orgSchedule->id);
        $this->assertModelData($fakeOrgSchedule, $dbOrgSchedule->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_org_schedule()
    {
        $orgSchedule = OrgSchedule::factory()->create();

        $resp = $this->orgScheduleRepo->delete($orgSchedule->id);

        $this->assertTrue($resp);
        $this->assertNull(OrgSchedule::find($orgSchedule->id), 'OrgSchedule should not exist in DB');
    }
}
