<?php namespace Tests\Repositories;

use App\Models\Major;
use App\Repositories\MajorRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MajorRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MajorRepository
     */
    protected $majorRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->majorRepo = \App::make(MajorRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_major()
    {
        $major = Major::factory()->make()->toArray();

        $createdMajor = $this->majorRepo->create($major);

        $createdMajor = $createdMajor->toArray();
        $this->assertArrayHasKey('id', $createdMajor);
        $this->assertNotNull($createdMajor['id'], 'Created Major must have id specified');
        $this->assertNotNull(Major::find($createdMajor['id']), 'Major with given id must be in DB');
        $this->assertModelData($major, $createdMajor);
    }

    /**
     * @test read
     */
    public function test_read_major()
    {
        $major = Major::factory()->create();

        $dbMajor = $this->majorRepo->find($major->id);

        $dbMajor = $dbMajor->toArray();
        $this->assertModelData($major->toArray(), $dbMajor);
    }

    /**
     * @test update
     */
    public function test_update_major()
    {
        $major = Major::factory()->create();
        $fakeMajor = Major::factory()->make()->toArray();

        $updatedMajor = $this->majorRepo->update($fakeMajor, $major->id);

        $this->assertModelData($fakeMajor, $updatedMajor->toArray());
        $dbMajor = $this->majorRepo->find($major->id);
        $this->assertModelData($fakeMajor, $dbMajor->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_major()
    {
        $major = Major::factory()->create();

        $resp = $this->majorRepo->delete($major->id);

        $this->assertTrue($resp);
        $this->assertNull(Major::find($major->id), 'Major should not exist in DB');
    }
}
