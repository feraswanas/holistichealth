<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\OrgSchedule;

class OrgScheduleApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_org_schedule()
    {
        $orgSchedule = OrgSchedule::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/org_schedules', $orgSchedule
        );

        $this->assertApiResponse($orgSchedule);
    }

    /**
     * @test
     */
    public function test_read_org_schedule()
    {
        $orgSchedule = OrgSchedule::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/org_schedules/'.$orgSchedule->id
        );

        $this->assertApiResponse($orgSchedule->toArray());
    }

    /**
     * @test
     */
    public function test_update_org_schedule()
    {
        $orgSchedule = OrgSchedule::factory()->create();
        $editedOrgSchedule = OrgSchedule::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/org_schedules/'.$orgSchedule->id,
            $editedOrgSchedule
        );

        $this->assertApiResponse($editedOrgSchedule);
    }

    /**
     * @test
     */
    public function test_delete_org_schedule()
    {
        $orgSchedule = OrgSchedule::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/org_schedules/'.$orgSchedule->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/org_schedules/'.$orgSchedule->id
        );

        $this->response->assertStatus(404);
    }
}
