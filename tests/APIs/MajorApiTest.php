<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Major;

class MajorApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_major()
    {
        $major = Major::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/majors', $major
        );

        $this->assertApiResponse($major);
    }

    /**
     * @test
     */
    public function test_read_major()
    {
        $major = Major::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/majors/'.$major->id
        );

        $this->assertApiResponse($major->toArray());
    }

    /**
     * @test
     */
    public function test_update_major()
    {
        $major = Major::factory()->create();
        $editedMajor = Major::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/majors/'.$major->id,
            $editedMajor
        );

        $this->assertApiResponse($editedMajor);
    }

    /**
     * @test
     */
    public function test_delete_major()
    {
        $major = Major::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/majors/'.$major->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/majors/'.$major->id
        );

        $this->response->assertStatus(404);
    }
}
