<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\DoctorOrg;

class DoctorOrgApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_doctor_org()
    {
        $doctorOrg = DoctorOrg::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/doctor_orgs', $doctorOrg
        );

        $this->assertApiResponse($doctorOrg);
    }

    /**
     * @test
     */
    public function test_read_doctor_org()
    {
        $doctorOrg = DoctorOrg::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/doctor_orgs/'.$doctorOrg->id
        );

        $this->assertApiResponse($doctorOrg->toArray());
    }

    /**
     * @test
     */
    public function test_update_doctor_org()
    {
        $doctorOrg = DoctorOrg::factory()->create();
        $editedDoctorOrg = DoctorOrg::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/doctor_orgs/'.$doctorOrg->id,
            $editedDoctorOrg
        );

        $this->assertApiResponse($editedDoctorOrg);
    }

    /**
     * @test
     */
    public function test_delete_doctor_org()
    {
        $doctorOrg = DoctorOrg::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/doctor_orgs/'.$doctorOrg->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/doctor_orgs/'.$doctorOrg->id
        );

        $this->response->assertStatus(404);
    }
}
