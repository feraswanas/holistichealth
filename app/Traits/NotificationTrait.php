<?php

namespace App\Traits;
use Illuminate\Http\Request;
use App\Models\User;
use Response;
use App\Models\Notification;
use Illuminate\Support\Facades\Auth;
trait NotificationTrait {
    public function saveNotification($body,$case,$user_id,$title,$image,$longitude,$lat)
    {
         
        $notification= new Notification;
          $notification->user_id=$user_id;
          $notification->body=$body;
          $notification->case=$case;
          $notification->title=$title;
          $notification->image=$image;
          $notification->lat=$lat;
          $notification->longitude=$longitude;

        //   $notification->description=json_encode($json,JSON_UNESCAPED_UNICODE);
          $notification->save();
       
    }
    public function sendNotification($user_id,$title,$case,$device_token,$body,$image,$longitude,$lat)
    {
      if($case != 6)
       $this->saveNotification($body,$case,$user_id,$title,$image,$longitude,$lat);
        $token = $device_token;
        
        // $token = "dzFluFo9GEyTtg48xEMKYE:APA91bEliAuq5vKlg_lP0zDn570DvemyY68_KuDOwXrJ7_homKuqPXwsi5dHfK_SpxKduQMUmhgDR9tCkY8HbtMBJEtDYBxfzuM62fiWj0uGkF5PQ6DTY6GryO9VjgmhwU9IzJ7c7eZz";  

       $from = "AAAA_Nn_8cw:APA91bHPYHrUtOmw_1AexQ6gIM5pF0Lb0daiiNULRbP73YzJBIMzJgnPuYNinPg2_8EYu97jsPlyUHp2hQrWp3TzZk6N2hV2Rlo9cTnWtG-kb7S7f5KEqup3O2Jso0PqzAuXZhqKHidi";
        $msg = array
              (
               'body'  => $body,
                'title' => $title,
                'receiver' => $user_id,
                'case' => $case,
                'longitude'=>$longitude,
                'image'=>$image,
                'lat'=>$lat,
                'icon'  => "https://image.flaticon.com/icons/png/512/270/270014.png",/*Default Icon*/
                'sound' => 'mySound'/*Default sound*/
              );

        $fields = array
                (
                    'to'        => $token,
                    'notification'  => $msg,
                    'data' => $msg
                );

        $headers = array
                (
                    'Authorization: key=' . $from,
                    'Content-Type: application/json'
                );
        //#Send Reponse To FireBase Server 
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
    }


}