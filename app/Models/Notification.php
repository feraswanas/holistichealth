<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Notification
 * @package App\Models
 * @version June 14, 2022, 12:39 pm UTC
 *
 * @property string $user_id
 * @property integer $seen
 * @property integer $case
 * @property string $body
 * @property string $title
 */
class Notification extends Model
{
    // use SoftDeletes;

    use HasFactory;

    public $table = 'notifications';
    

    // protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'seen',
        'case',
        'body',
        'title',
        'image',
        'lat',
        'longitude'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'string',
        'seen' => 'string',
        'case' => 'integer',
        'body' => 'string',
        'title' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

     
}
