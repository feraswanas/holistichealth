<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use GoldSpecDigital\LaravelEloquentUUID\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
/**
 * Class Patient
 * @package App\Models
 * @version February 25, 2022, 8:37 am UTC
 *
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $password
 * @property string $birth_date
 * @property string $gender
 * @property string $id_number
 * @property string $blood_type
 * @property string $cronic
 * @property string $qr
 * @property string $qr_constant
 * @property string $picture
 * @property integer $user_type
 */
class Patient extends Authenticatable
{
    // use SoftDeletes;

    use HasApiTokens, HasFactory, Notifiable;

    public $table = 'patients';
    

    protected $dates = ['deleted_at'];



    
    public $hidden = [
       'created_at',
       'updated_at',

        'password',

    ];
    public $fillable = [
        'name',
        'email',
        'phone',
        'password',
        'birth_date',
        'gender',
        'id_number',
        'blood_type',
        'cronic',
        'qr',
        'qr_constant',
        'picture',
        'emergency1',
        'emergency2',
        'emergency3',
        'user_type',
        'device_token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'password' => 'string',
        'birth_date' => 'string',
        'gender' => 'string',
        'id_number' => 'string',
        'blood_type' => 'string',
        'cronic' => 'string',
        'qr' => 'string',
        'qr_constant' => 'string',
        'picture' => 'string',
        'emergency1' => 'string',
        'emergency2' => 'string',
        'emergency3' => 'string',
        'user_type' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'email|required|unique:patients',
        'phone' => 'required|unique:patients|numeric',
        'password' => 'required',
        'birth_date' => 'required',
        'gender' => 'required',
        'id_number' => 'required|numeric|unique:patients',
        'blood_type' => 'required'
    ];
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function history()
    {
       return $this->hasMany(History::class,'patient_id');
    }
    public function family()
    {
       return $this->hasMany(Family::class,'user_id1');
    }
    public function family2()
    {
       return $this->hasMany(Family::class,'user_id2');
    }

    public function emergency_1()
    {
       return $this->belongsTo(Patient::class,'emergency1');
    }

    public function emergency_2()
    {
       return $this->belongsTo(Patient::class,'emergency2');
    }
    public function emergency_3()
    {
       return $this->belongsTo(Patient::class,'emergency3');
    }

    public function notifications()
    {
       return $this->hasMany(Notification::class,'user_id');
    }

    // public function getRelation()
    // {
    //     $id=Auth::id();

    //     $family=Family::where('user_id1',$id)->get();
    //    return $this->hasMany(Family::class,'user_id1');
        
    // }
    
}
