<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Rating
 * @package App\Models
 * @version March 2, 2022, 8:08 am UTC
 *
 * @property string $doctor_id
 * @property string $patient_id
 * @property number $rate
 * @property string $review
 * @property string $date
 */
class Rating extends Model
{
    // use SoftDeletes;

    use HasFactory;

    public $table = 'ratings';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'doctor_id',
        'patient_id',
        'rate',
        'review',
        'date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'doctor_id' => 'string',
        'patient_id' => 'string',
        'rate' => 'float',
        'review' => 'string',
        'date' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'doctor_id' => 'required|exists:doctors,id',
        'patient_id' => 'required|exists:patients,id',
        'rate' => 'required|numeric|min:0|max:5'
    ];

    public function doctor()
    {
       return $this->belongsTo(Doctor::class);
    }

    
}
