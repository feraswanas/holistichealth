<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Major
 * @package App\Models
 * @version May 23, 2022, 3:15 pm UTC
 *
 * @property string $major
 */
class Major extends Model
{
    // use SoftDeletes;

    use HasFactory;

    public $table = 'majors';
    

    // protected $dates = ['deleted_at'];



    public $fillable = [
        'major',
        'drug'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'major' => 'string',
        'drug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
