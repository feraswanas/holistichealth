<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Operation
 * @package App\Models
 * @version May 21, 2022, 5:32 pm UTC
 *
 * @property string $doctor1_id
 * @property string $doctor2_id
 * @property string $doctor3_id
 * @property string $patient_id
 * @property string $organization_id
 * @property string $description
 * @property string $status
 * @property string $date
 */
class Operation extends Model
{
    // use SoftDeletes;

    use HasFactory;

    public $table = 'operations';
    

    // protected $dates = ['deleted_at'];



    public $fillable = [
        'doctor1_id',
        'doctor2_id',
        'doctor3_id',
        'patient_id',
        'organization_id',
        'description',
        'status',
        'date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'doctor1_id' => 'string',
        'doctor2_id' => 'string',
        'doctor3_id' => 'string',
        'patient_id' => 'string',
        'organization_id' => 'string',
        'description' => 'string',
        'status' => 'string',
        'date' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'doctor1_id' => 'required|exists:doctors,id',
        'doctor2_id' => 'exists:doctors,id',
        'doctor3_id' => 'exists:doctors,id',
        'patient_id' => 'required|exists:patients,id',
        // 'organization_id' => 'required',
        'description' => 'required',
        'date' => 'required'
    ];

    public function doctor1()
    {
       return $this->belongsTo(Doctor::class,'doctor1_id');
    }
    
    public function doctor2()
    {
       return $this->belongsTo(Doctor::class,'doctor2_id');
    }

    public function doctor3()
    {
       return $this->belongsTo(Doctor::class,'doctor3_id');
    }

    public function organization()
    {
       return $this->belongsTo(Organization::class,'organization_id');
    }

    public function patient()
    {
       return $this->belongsTo(Patient::class,'patient_id');
    }
    
}
