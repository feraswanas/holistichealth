<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Laravel\Passport\HasApiTokens;
use GoldSpecDigital\LaravelEloquentUUID\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Class Organization
 * @package App\Models
 * @version April 1, 2022, 7:05 pm UTC
 *
 * @property string $status
 * @property string $name
 * @property string $image
 * @property string $email
 * @property string $password
 * @property string $phone1
 * @property string $phone2
 * @property string $location
 * @property string $tax
 * @property string $device_token
 * @property string $major
 */
class Organization extends Authenticatable
{
    // use SoftDeletes;

    // use HasFactory;
    use HasApiTokens, HasFactory, Notifiable;

    public $table = 'organizations';
    

    // protected $dates = ['deleted_at'];



    public $fillable = [
        'status',
        'name',
        'image',
        'email',
        'password',
        'phone1',
        'phone2',
        'lat',
        'long',
        'tax',
        'device_token',
        'major',
        'address',
        'org_type',
        'user_type',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'status' => 'string',
        'name' => 'string',
        'image' => 'string',
        'email' => 'string',
        'password' => 'string',
        'phone1' => 'string',
        'phone2' => 'string',
        'lat' => 'string',
        'long' => 'string',
        'tax' => 'string',
        'device_token' => 'string',
        'major' => 'string',
        'address' => 'string',
        'description' => 'string',
        'org_type'=>'string'
        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required|unique:doctors|unique:organizations',
        'password' => 'required',
        'phone1' => 'required|unique:doctors,phone|unique:organizations,phone1|unique:organizations,phone2',
        'phone2' => 'unique:doctors,phone|unique:organizations,phone1|unique:organizations,phone2',
        'lat'=>'required',
        'long'=>'required',
        'address'=>'required',
        'org_type'=>"required",
        'image'=>'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        'tax' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        'major'=>'required'
        
    ];


    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }
    
    public function doctors()
    {
       return $this->hasMany(DoctorOrg::class,'org_id');
    }

    public function notifications()
    {
       return $this->hasMany(Notification::class,'user_id');
    }
    
}
