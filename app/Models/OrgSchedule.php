<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class OrgSchedule
 * @package App\Models
 * @version May 10, 2022, 5:03 pm UTC
 *
 * @property integer $doctor_org_id
 * @property string $day
 * @property string $from
 * @property string $to
 */
class OrgSchedule extends Model
{
    // use SoftDeletes;

    use HasFactory;

    public $table = 'org_schedules';
    

    // protected $dates = ['deleted_at'];



    public $fillable = [
        'doctor_org_id',
        'day',
        'from',
        'to'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'doctor_org_id' => 'integer',
        'day' => 'string',
        'from' => 'string',
        'to' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    'doctor_org_id' => 'required|exists:doctor_orgs,id',
        'day' => 'required',
        'from' => 'required',
        'to' => 'required'
    ];

    public function doctorOrg()
    {
       return $this->belongsTo(DoctorOrg::class,'doctor_org_id');
    }

    
}
