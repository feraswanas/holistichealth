<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class DoctorOrg
 * @package App\Models
 * @version April 27, 2022, 11:50 pm UTC
 *
 * @property string $doctor_id
 * @property string $org_id
 * @property string $from
 * @property string $status
 */
class DoctorOrg extends Model
{
    // use SoftDeletes;

    use HasFactory;

    public $table = 'doctor_orgs';
    

    // protected $dates = ['deleted_at'];



    public $fillable = [
        'doctor_id',
        'org_id',
        'from',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'doctor_id' => 'string',
        'org_id' => 'string',
        'from' => 'string',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'doctor_id'=>'exists:doctors,id',
        'org_id'=>'exists:organizations,id'
        
    ];


    
    public function doctor()
    {
       return $this->belongsTo(Doctor::class,'doctor_id');
    }

    public function organization()
    {
       return $this->belongsTo(Organization::class,'org_id');
    }
    public function schedules()
    {
       return $this->hasMany(OrgSchedule::class,'doctor_org_id');
    }
    
}
