<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use GoldSpecDigital\LaravelEloquentUUID\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
/**
 * Class Doctor
 * @package App\Models
 * @version February 23, 2022, 8:13 am UTC
 *
 * @property string $doctor
 * @property string $email
 * @property string $password
 * @property string $birth_date
 * @property string $gender
 * @property string $id_number
 * @property string $phone
 * @property string $major
 * @property string $certificate
 * @property string $device_token
 * @property integer $user_type
 */
class Doctor extends Authenticatable
{
    // use SoftDeletes;

    use HasApiTokens, HasFactory, Notifiable;

    public $table = 'doctors';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'email',
        'password',
        'birth_date',
        'gender',
        'id_number',
        'phone',
        'major',
        'rate',
        'certificate',
        'device_token',
        'user_type',
        'description',
        'status',
        'picture'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'birth_date' => 'string',
        'gender' => 'string',
        'id_number' => 'string',
        'phone' => 'string',
        'major' => 'string',
        'rate'=>'float',
        'description'=>'string',
        'certificate' => 'string',
        'device_token' => 'string',
        'user_type' => 'integer',
        'status' => 'string',
        'picture'=>'string'

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'email|required|unique:doctors|unique:organizations',
        // 'password' => 'required',
        'birth_date' => 'required',
        'gender' => 'required',
        'id_number' => 'required|numeric|unique:doctors',
        'password' => [
            'required',
            'string',
            'min:8',             // must be at least 10 characters in length
            'regex:/[a-z]/',      // must contain at least one lowercase letter
            'regex:/[A-Z]/',      // must contain at least one uppercase letter
            'regex:/[0-9]/',      // must contain at least one digit
            'regex:/[@$!%*#?&]/', // must contain a special character
        ],
        'phone' => 'required|numeric|unique:doctors,phone|unique:organizations,phone1|unique:organizations,phone2',
        'major' => 'required',
        'certificate' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        'device_token' => ''
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }
    

    public function history()
    {
       return $this->hasMany(History::class,'doctor_id');
    }

    public function ratings()
    {
       return $this->hasMany(Rating::class,'doctor_id');
    }

    public function organizations()
    {
       return $this->hasMany(DoctorOrg::class,'doctor_id');
    }

    public function operations()
    {
       return $this->hasMany(Operation::class,'doctor1_id');
    }

    public function notifications()
    {
       return $this->hasMany(Notification::class,'user_id');
    }

}
