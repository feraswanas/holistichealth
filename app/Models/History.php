<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class History
 * @package App\Models
 * @version March 1, 2022, 7:10 pm UTC
 *
 * @property string $doctor_id
 * @property string $patient_id
 * @property string $description
 * @property string $prescription
 * @property string $chronic
 * @property string $priority
 * @property string $date
 */
class History extends Model
{
    // use SoftDeletes;

    use HasFactory;

    public $table = 'histories';
    

    protected $dates = ['deleted_at'];

 
    public $hidden = [
        'created_at',
        'updated_at',
 
         'deleted_at',
 
     ];

    public $fillable = [
        'doctor_id',
        'patient_id',
        'description',
        'prescription',
        'chronic',
        'priority',
        'date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'doctor_id' => 'string',
        'patient_id' => 'string',
        'description' => 'string',
        'prescription' => 'string',
        'chronic' => 'string',
        'priority' => 'string',
        'date' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'doctor_id' => 'required',
        'patient_id' => 'required',
        'description' => 'required',
        'prescription' => 'required',
        'chronic' => 'required',
        'priority' => 'required'
    ];
    public function doctor()
    {
       return $this->belongsTo(Doctor::class);
    }
     public function patient()
    {
       return $this->belongsTo(Patient::class,'patient_id');
    }
    
}
