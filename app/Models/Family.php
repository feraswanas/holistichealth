<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Family
 * @package App\Models
 * @version March 17, 2022, 2:37 pm UTC
 *
 * @property string $user_id1
 * @property string $user_id2
 * @property string $relation
 * @property string $status
 */
class Family extends Model
{
    // use SoftDeletes;

    use HasFactory;

    public $table = 'families';
    

    // protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id1',
        'user_id2',
        'relation',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id1' => 'string',
        'user_id2' => 'string',
        'relation' => 'string',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        // 'user_id1' => 'required|exists:patients,id',
        'user_id2' => 'required|exists:patients,id',
        'relation' => 'required'
    ];


    public function patient1()
    {
       return $this->belongsTo(Patient::class,'user_id1');
    } 
    
    public function patient2()
    {
       return $this->belongsTo(Patient::class,'user_id2');
    }

    
}
