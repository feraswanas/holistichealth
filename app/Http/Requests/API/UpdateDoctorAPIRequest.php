<?php

namespace App\Http\Requests\API;

use App\Models\Doctor;
use InfyOm\Generator\Request\APIRequest;
use Illuminate\Validation\Rule;

class UpdateDoctorAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $rules = [
            'phone' => ['numeric',
            // Rule::unique('patients')->ignore($this->user()->id),
            Rule::unique('organizations','phone1')->ignore($this->user()->id),
            Rule::unique('organizations','phone2')->ignore($this->user()->id),
            Rule::unique('doctors')->ignore($this->user()->id),

],
            'id_number' => ['numeric',
            // Rule::unique('patients')->ignore($this->user()->id),
            // Rule::unique('organizations')->ignore($this->user()->id),
            Rule::unique('doctors')->ignore($this->user()->id),

],
            'email' => ['email',
            // Rule::unique('patients')->ignore($this->user()->id),
            Rule::unique('organizations')->ignore($this->user()->id),
            Rule::unique('doctors')->ignore($this->user()->id),


],
            // 'id_picture'=> 'image|mimes:jpg,png,jpeg,gif,svg',
            'picture' =>'image|mimes:jpg,png,jpeg,gif,svg'
        ];
    }
}
