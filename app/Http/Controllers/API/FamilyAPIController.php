<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFamilyAPIRequest;
use App\Http\Requests\API\UpdateFamilyAPIRequest;
use App\Models\Family;
use App\Repositories\FamilyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Traits\NotificationTrait;
use Response;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Family\RequestResource;

/**
 * Class FamilyController
 * @package App\Http\Controllers\API
 */

class FamilyAPIController extends AppBaseController
{
    use NotificationTrait;

    /** @var  FamilyRepository */
    private $familyRepository;

    public function __construct(FamilyRepository $familyRepo)
    {
        $this->familyRepository = $familyRepo;
    }

    /**
     * Display a listing of the Family.
     * GET|HEAD /families
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $families = $this->familyRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($families->toArray(), 'Families retrieved successfully');
    }

    /**
     * Store a newly created Family in storage.
     * POST /families
     *
     * @param CreateFamilyAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateFamilyAPIRequest $request)
    {
        $id=Auth::id();
        $input = $request->all();

        $input['user_id1']=$id;
        $family=Family::where('user_id1',$input['user_id1'])->where('user_id2',$input['user_id2'])->first();      

        if($family)
        return $this->sendError('Request sent before');
        $family=Family::where('user_id2',$input['user_id1'])->where('user_id1',$input['user_id2'])->first();      
 
        if($family)
        return $this->sendError('Request sent before');
        if($input['user_id1'] == $input['user_id2'])
        return $this->sendError('ERROR');

        $family = $this->familyRepository->create($input);
        $patient2=$family->patient2;
        $patient1=$family->patient1;


        $this->sendNotification($input['user_id2'],"NEW REQUEST",3,$patient2->device_token,'you have got a new family request from '.$patient1->name,$patient1->picture,0,0);
        unset($family['patient2']);
        unset($family['patient1']);

        return $this->sendResponse($family->toArray(), 'Family saved successfully');
    }


    public function getRequest()
    {
        $id=Auth::id();
        $requests=Family::where('user_id2',$id)->where('status','PENDING')->get();
        foreach ($requests as $request)
        {
            if($id == $request->user_id2 && $request->relation == 'Parent')
            $request->relation = 'Child';
            if($id == $request->user_id2 && $request->relation == 'Child')
            $request->relation = 'Parent';
        }
        return Response::json(['requests'=>RequestResource::collection($requests)]);
        
        
    }
    public function accept(Request $request)
    {
        $validated = $request->validate([
            'id' => 'required|exists:families',
            ]);
         $family=Family::where('id',$request->id)->first();      
         $family->update(['status'=>'ACCEPTED']);
         $patient1=$family->patient1;
         $patient2=$family->patient2;

        $this->sendNotification($patient1->id,"REQUEST ACCEPTED",0,$patient1->device_token,'your  family request with '.$patient2->name.' is accepted',$patient2->picture,0,0);

        return $this->sendSuccess('Request accepted');
    }

    public function reject(Request $request)
    {
        $validated = $request->validate([
            'id' => 'required|exists:families',
            ]);
         $family=Family::where('id',$request->id)->delete();      
        // $this->sendNotification($family->patient1->id,"REQUEST REJECTED",0,$family->patient1->device_token,'your  family request from '.$family->patient1->name.' is rejected',$family->patient1->picture,'','');

        //  $family->update(['status'=>'ACCEPTED']);
        return $this->sendSuccess('Request rejected');
    }
    public function getFamily()
    {
        $id=Auth::id();
        
        $accepted=Family::where('status','ACCEPTED')->get();  
        $accepted1 = $accepted->where('user_id1',$id);
        $accepted2 = $accepted->where('user_id2',$id);
        foreach ($accepted2 as $request)
        {
            if($id == $request->user_id2 && $request->relation == 'Parent')
            $request->relation = "Child";
            else if($id == $request->user_id2 && $request->relation == 'Child')
            $request->relation = 'Parent';
        }
        $accepted = $accepted1->merge($accepted2);
       return Response::json(['requests'=>RequestResource::collection($accepted)]);

        
    }

    
    
    /**
     * Display the specified Family.
     * GET|HEAD /families/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Family $family */
        $family = $this->familyRepository->find($id);

        if (empty($family)) {
            return $this->sendError('Family not found');
        }

        return $this->sendResponse($family->toArray(), 'Family retrieved successfully');
    }

    /**
     * Update the specified Family in storage.
     * PUT/PATCH /families/{id}
     *
     * @param int $id
     * @param UpdateFamilyAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFamilyAPIRequest $request)
    {
        $input = $request->all();

        /** @var Family $family */
        $family = $this->familyRepository->find($id);

        if (empty($family)) {
            return $this->sendError('Family not found');
        }

        $family = $this->familyRepository->update($input, $id);

        return $this->sendResponse($family->toArray(), 'Family updated successfully');
    }

    /**
     * Remove the specified Family from storage.
     * DELETE /families/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Family $family */
        $family = $this->familyRepository->find($id);

        if (empty($family)) {
            return $this->sendError('Family not found');
        }

        $family->delete();

        return $this->sendSuccess('Family deleted successfully');
    }
}
