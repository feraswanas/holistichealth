<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOrgScheduleAPIRequest;
use App\Http\Requests\API\UpdateOrgScheduleAPIRequest;
use App\Models\OrgSchedule;
use App\Models\DoctorORg;

use App\Repositories\OrgScheduleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Http\Resources\DoctorOrg\DoctorScheduleResource;
use App\Http\Resources\DoctorOrg\ScheduleResource;
use Illuminate\Support\Facades\Auth;    

/**
 * Class OrgScheduleController
 * @package App\Http\Controllers\API
 */

class OrgScheduleAPIController extends AppBaseController
{
    /** @var  OrgScheduleRepository */
    private $orgScheduleRepository;

    public function __construct(OrgScheduleRepository $orgScheduleRepo)
    {
        $this->orgScheduleRepository = $orgScheduleRepo;
    }

    /**
     * Display a listing of the OrgSchedule.
     * GET|HEAD /orgSchedules
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $orgSchedules = $this->orgScheduleRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($orgSchedules->toArray(), 'Org Schedules retrieved successfully');
    }

    public function getDoctor(Request $request)
    {
        $validated = $request->validate([
            'doctor_id' => 'required|exists:doctors,id',

            ]);
        $schedules=OrgSchedule::orderBy('day')->get();
        $doctor_orgs=$schedules->load('doctorOrg');
        $doctor_orgs=$doctor_orgs->where('doctorOrg.doctor_id',$request->doctor_id);
        return Response::json(['relations'=> DoctorScheduleResource::collection($doctor_orgs)]);
        
        return $doctor_orgs;
    }

    public function getScheduleWithoutOrg(Request $request)
    {
        $validated = $request->validate([
            'doctor_id' => 'required|exists:doctors,id',
            ]);
        $schedules=OrgSchedule::orderBy('day')->get();
        $doctor_orgs=$schedules->load('doctorOrg');
        $doctor_orgs=$doctor_orgs->where('doctorOrg.doctor_id',$request->doctor_id)->where('doctorOrg.org_id',$request->org_id);
        return Response::json(['relations'=> ScheduleResource::collection($doctor_orgs)]);
        
        return $doctor_orgs;
    }

    public function checkSchedule(Request $request)
    {
        $user=Auth::user();
        $schedules=OrgSchedule::orderBy('day')->get();
        $doctor_orgs=$schedules->load('doctorOrg');
        $doctor_orgs=$doctor_orgs->where('doctorOrg.doctor_id',$user->id)->wher;
        return $doctor_orgs;
        return Response::json(['relations'=> ScheduleResource::collection($doctor_orgs)]);
        
        return $doctor_orgs;
    }


    /**
     * Store a newly created OrgSchedule in storage.
     * POST /orgSchedules
     *
     * @param CreateOrgScheduleAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateOrgScheduleAPIRequest $request)
    {
       
        $input = $request->all();
        $user=Auth::user();
         $adding30Minutes= strtotime($request->from . '+ 30 minute');
        $added30Min=date('H:i', $adding30Minutes);
        $schedules=OrgSchedule::orderBy('day')->get();
        $doctor_orgs=$schedules->load('doctorOrg');
        $doctor_orgs=$doctor_orgs->where('doctorOrg.doctor_id',$user->id)->where('day',$request->day);
        
        if($added30Min >= $request->to)
            return Response::json(['message'=> 'short time slot']);
        
        if($request->from > $request->to)
            return Response::json(['message'=> 'to must be bigger than from']);
        foreach ($doctor_orgs as $doctor_org)
        {
            if($request->from <= $doctor_org->to)
            {
                 if($request->from > $doctor_org->from)
                 {
                      return Response::json(['message'=> 'This time slot is making conflict with your schedule']);
                 }
            }

            if($request->to <= $doctor_org->to)
            {
                 if($request->to > $doctor_org->from)
                 {
                      return Response::json(['message'=> 'This time slot is making conflict with your schedule']);
                 }
            }

        }
        // return $doctor_orgs;

        $orgSchedule = $this->orgScheduleRepository->create($input);
 
        return $this->sendResponse($orgSchedule->toArray(), 'Org Schedule saved successfully');
    }

    /**
     * Display the specified OrgSchedule.
     * GET|HEAD /orgSchedules/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var OrgSchedule $orgSchedule */
        $orgSchedule = $this->orgScheduleRepository->find($id);

        if (empty($orgSchedule)) {
            return $this->sendError('Org Schedule not found');
        }

        return $this->sendResponse($orgSchedule->toArray(), 'Org Schedule retrieved successfully');
    }

    /**
     * Update the specified OrgSchedule in storage.
     * PUT/PATCH /orgSchedules/{id}
     *
     * @param int $id
     * @param UpdateOrgScheduleAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrgScheduleAPIRequest $request)
    {
        $input = $request->all();

        /** @var OrgSchedule $orgSchedule */
        $orgSchedule = $this->orgScheduleRepository->find($id);

        if (empty($orgSchedule)) {
            return $this->sendError('Org Schedule not found');
        }

        $orgSchedule = $this->orgScheduleRepository->update($input, $id);

        return $this->sendResponse($orgSchedule->toArray(), 'OrgSchedule updated successfully');
    }

    /**
     * Remove the specified OrgSchedule from storage.
     * DELETE /orgSchedules/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        /** @var OrgSchedule $orgSchedule */
        $validated = $request->validate([
            'id' => 'required|exists:org_schedules,id',
            ]);
        $orgSchedule = $this->orgScheduleRepository->find($request->id);

        if (empty($orgSchedule)) {
            return $this->sendError('Org Schedule not found');
        }

        $orgSchedule->delete();

        return $this->sendSuccess('Org Schedule deleted successfully');
    }
}
