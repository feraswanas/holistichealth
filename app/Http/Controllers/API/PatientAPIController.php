<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePatientAPIRequest;
use App\Http\Requests\API\UpdatePatientAPIRequest;
use App\Models\Patient;
use App\Models\Operation;

use App\Repositories\PatientRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Support\Str;
use App\Http\Resources\Patient\PatientResource;
use App\Http\Resources\Patient\PatientUpdateResource;
use App\Http\Resources\Patient\PatientHistoryResource;
use App\Http\Resources\Patient\PatientEmergencyResource;
use App\Http\Resources\Operation\OperationResource;

use Illuminate\Support\Facades\Crypt;
use App\Models\Family;
use App\Http\Resources\Family\RequestResource;
use App\Traits\NotificationTrait;

use Illuminate\Support\Facades\Auth;


/**
 * Class PatientController
 * @package App\Http\Controllers\API
 */

class PatientAPIController extends AppBaseController
{
    use NotificationTrait;

    /** @var  PatientRepository */
    private $patientRepository;

    public function __construct(PatientRepository $patientRepo)
    {
        $this->patientRepository = $patientRepo;
    }

    /**
     * Display a listing of the Patient.
     * GET|HEAD /patients
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $patients = $this->patientRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($patients->toArray(), 'Patients retrieved successfully');
    }

    /**
     * Store a newly created Patient in storage.
     * POST /patients
     *
     * @param CreatePatientAPIRequest $request
     *
     * @return Response
     */

    public function store(CreatePatientAPIRequest $request)
    {
        $input = $request->all();

        if($request->hasFile('picture')){
            $name = time(). "." .$request->file('picture')->getClientOriginalName();
            $request->picture->move(public_path()."/images/patient/" , $name);
            $input['picture'] = url(asset('public/images/patient/'.$name));         
            }  
            $time=time();
            $random = Str::random(25);
           $qr = Str::start("",$random);
           $input['qr_constant'] = Str::start($time,$qr);
        // return $input;

           if($input['cronic'] !=null){
           foreach($input['cronic'] as $cronic){

            $json[]=$cronic;
    
            }
            $input['cronic'] = json_encode($json,JSON_UNESCAPED_UNICODE);
        }
        if($input['cronic'] ==[null])
        $input['cronic']= null;
            $patient = $this->patientRepository->create($input);
            // $patient->fill([
            //     'name' => Crypt::encryptString($request->name),
            //     'id_number' => Crypt::encryptString($request->id_number),
            //     'email' => Crypt::encryptString($request->email),
            //     'phone' => Crypt::encryptString($request->phone),

            // ])->save();
        $patient->token = $patient->createToken('patient')->accessToken;
        // $patient->name = Crypt::decryptString($patient->name);
        return Response::json(['user'=> new PatientResource($patient)]);
            // return $this->sendResponse($patient->toArray(), 'Patient saved successfully');
        
    }
    public function login(Request $req)
    {
        $validated = $req->validate([
            'email_phone'=>'required',
            'password'=>'required|min:6',
            'device_token'=>'required'
        ]);
        // return $req->device_token;
        if ( Auth::guard('patient')->attempt(['email' => $req->email_phone  , 'password' => $req->password] )|| Auth::guard('patient')->attempt(['phone' => $req->email_phone  , 'password' => $req->password] )) {
            $patient = auth()->guard('patient')->user();
            
            $patient->device_token=$req->device_token;
            $patient->save();
            $patient->token = $patient->createToken('patient')->accessToken;
          return Response::json(['user'=> new PatientResource($patient)]);
    
        } 
        else{
        return Response::json(['message'=>"incorrect"]);
        }
    }

    public function checkPassword(Request $req)
    {
        $validated = $req->validate([
            'password'=>'required',
        ]);
        $patient=Auth::user();
        if ( Auth::guard('patient')->attempt(['email' => $patient->email  , 'password' => $req->password] )) {
          return Response::json(['message'=> 'correct password']);
        }
        return Response::json(['message'=> 'incorrect password']);

    }

    public function details()
    {
        $patient=Auth::user();
        return Response::json(['user'=> new PatientUpdateResource($patient)]);
        
    }
 

    public function qr()
    {
        $user=Auth::user();
        $time=time();
        $random = Str::random(25);
        $qr = Str::start("",$random);
        $qr = Str::start($time,$qr);
        $user->update(['qr'=>$qr]);
        return Response::json(['user'=> new PatientUpdateResource($user)]);
           
    }

    public function getAll()
    {
        $id=Auth::id();
        $patients=Patient::all();
        $accepted=Family::where('status','ACCEPTED')->get();  
        $accepted1 = $accepted->where('user_id1',$id);
        $accepted2 = $accepted->where('user_id2',$id);
        $accepted = $accepted1->merge($accepted2);    
        $pending=Family::where('status','PENDING')->get();      
        $pending1 = $pending->where('user_id1',$id);
        $pending2 = $pending->where('user_id2',$id);
        $pending = $pending1->merge($pending2);
        return Response::json(['patients'=> PatientUpdateResource::collection($patients) , 'accepted'=>RequestResource::collection($accepted) , 'pending'=>RequestResource::collection($pending)]);

    }

    public function getQr(Request $request)
    {
        $validated = $request->validate([
            'qr'=>'required|exists:patients,qr'
        ]);
        $patient=Patient::where('qr',$request->qr)->first();
        $patient->update(['qr'=>null]);
        return Response::json(['user'=> new PatientUpdateResource($patient)]);

    }


    public function getQrConstant(Request $request)
    {
        $validated = $request->validate([
            'qr'=>'required|exists:patients,qr_constant'
        ]);
        $patient=Patient::where('qr_constant',$request->qr)->first();
        // $patient->update(['qr'=>null]);
        return Response::json(['user'=> new PatientEmergencyResource($patient)]);

    }
    
    public function history()
    {
        $user=Auth::user();
        return Response::json(['user'=> new PatientHistoryResource($user)]);

    }

    
    public function addEmergency(Request $request)
    {
        $validated = $request->validate([
            'patient_id'=>'required|exists:patients,id'
        ]);
        $patient=Auth::user();
        if($request->patient_id == $patient->id || $request->patient_id == $patient->emergency1 || $request->patient_id == $patient->emergency2 || $request->patient_id == $patient->emergency3)
        return Response::json(['message'=>"error"]);

        // $patient=Patient::where('id',$user->id)->first();
        if(!$patient->emergency1)
        {
        $patient->emergency1=$request->patient_id;
        $patient->save();
        $emergency= $patient->emergency_1;

        }
        elseif(!$patient->emergency2)
        {
        $patient->emergency2=$request->patient_id; 
        $patient->save();
        $emergency= $patient->emergency_2;

        }
        elseif(!$patient->emergency3)
        {
        $patient->emergency3=$request->patient_id;
        $patient->save();
        $emergency= $patient->emergency_3;

        }
        else 
        return Response::json(['message'=>"you have 3 contacts"]);

        $this->sendNotification($emergency->id,"EMERGENCY CONTACT",4,$emergency->device_token,$patient->name.' added you to their emergency contacts',$patient->picture,0,0);

        return Response::json(['message'=>"added successfully"]);
        
    }

    public function getOperations(Request $request)
    {
        $validated = $request->validate([
            'patient_id'=>'required|exists:patients,id'
        ]);
        $operations=Operation::where('patient_id',$request->patient_id)->where('status','ACCEPTED')->get();
        return Response::json(['operations'=> OperationResource::collection($operations)]);

    }

    public function getEmergency()
    {
        $user=Auth::user();
        return Response::json(['patient'=> PatientEmergencyResource::make($user)]);
    }

    public function emergency(Request $request)
    {
        $validated = $request->validate([
            'lat'=>'required',
            'longitude'=>'required'
        ]);
        $user=Auth::user();
        
        if($user->emergency_1 != null)
        {   
            $emergency1 = $user->emergency_1;
            $this->sendNotification($emergency1->id,"EMERGENCY CASE",5,$emergency1->device_token,$user->name.' is in emergency',$user->picture,$request->longitude,$request->lat);
        }

        if($user->emergency_2 != null)
        {   
            $emergency2 = $user->emergency_2;
            $this->sendNotification($emergency2->id,"EMERGENCY CASE",5,$emergency2->device_token,$user->name.' is in emergency',$user->picture,$request->longitude,$request->lat);
        }

        if($user->emergency_3 != null)
        {   
            $emergency3 = $user->emergency_3;
            $this->sendNotification($emergency3->id,"EMERGENCY CASE",5,$emergency3->device_token,$user->name.' is in emergency',$user->picture,$request->longitude,$request->lat);
        }

        return Response::json(['message'=>"emergency sent successfully"]);



    }
    public function removeEmergency(Request $request)
    {
        $user=Auth::user();

        $validated = $request->validate([
            'emergency'=>'required'
        ]);
        if($request->emergency == 'emergency1')
        $user->emergency1=null;

        if($request->emergency == 'emergency2')
        $user->emergency2=null;

        if($request->emergency == 'emergency3')
        $user->emergency3=null;

        $user->save();
        return Response::json(['message'=>"emergency contact deleted"]);




    }
    /**
     * Display the specified Patient.
     * GET|HEAD /patients/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Patient $patient */
        $patient = $this->patientRepository->find($id);

        if (empty($patient)) {
            return $this->sendError('Patient not found');
        }

        return $this->sendResponse($patient->toArray(), 'Patient retrieved successfully');
    }

    public function getIdNumber(Request $request)
    {
        $validated = $request->validate([
            'id_number'=>'required'
        ]);

        $patient=Patient::where('id_number',$request->id_number)->first();
        if($patient)
        return Response::json(['user'=> PatientEmergencyResource::make($patient)]);
        return $this->sendError('Patient not found');

    }

    public function logout(Request $req)
    {
        $user=Auth::user();
        $user->update(['device_token'=>null]);
        // $user->tokens->each(function($token, $key) {
        //     $token->delete();
        // });
        return Response::json(['message'=> "logout successfully"]);

    }


    /**
     * Update the specified Patient in storage.
     * PUT/PATCH /patients/{id}
     *
     * @param int $id
     * @param UpdatePatientAPIRequest $request
     *
     * @return Response
     */
    public function update(UpdatePatientAPIRequest $request)
    {
        $input = $request->all();
        $id=Auth::id();
        if($request->hasFile('picture')){
            $name = time(). "." .$request->file('picture')->getClientOriginalName();
            $request->picture->move(public_path()."/images/patient/" , $name);
            $input['picture'] = url(asset('public/images/patient/'.$name)); 
                    
            }  
            // return $input;
        /** @var Patient $patient */
        $patient = $this->patientRepository->find($id);

        

        $patient = $this->patientRepository->update($input, $id);
        return Response::json(['user'=> new PatientUpdateResource($patient)]);

        // return $this->sendResponse($patient->toArray(), 'Patient updated successfully');
    }


    /**
     * Remove the specified Patient from storage.
     * DELETE /patients/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Patient $patient */
        $patient = $this->patientRepository->find($id);

        if (empty($patient)) {
            return $this->sendError('Patient not found');
        }

        $patient->delete();

        return $this->sendSuccess('Patient deleted successfully');
    }
}
