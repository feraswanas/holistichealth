<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDoctorOrgAPIRequest;
use App\Http\Requests\API\UpdateDoctorOrgAPIRequest;
use App\Models\DoctorOrg;
use App\Models\Organization;
use App\Traits\NotificationTrait;

use App\Repositories\DoctorOrgRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\DoctorOrg\DoctorOrgResource;

/**
 * Class DoctorOrgController
 * @package App\Http\Controllers\API
 */

class DoctorOrgAPIController extends AppBaseController
{
    use NotificationTrait;
    
    /** @var  DoctorOrgRepository */
    private $doctorOrgRepository;

    public function __construct(DoctorOrgRepository $doctorOrgRepo)
    {
        $this->doctorOrgRepository = $doctorOrgRepo;
    }

    /**
     * Display a listing of the DoctorOrg.
     * GET|HEAD /doctorOrgs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $doctorOrgs = $this->doctorOrgRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($doctorOrgs->toArray(), 'Doctor Orgs retrieved successfully');
    }

    /**
     * Store a newly created DoctorOrg in storage.
     * POST /doctorOrgs
     *
     * @param CreateDoctorOrgAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDoctorOrgAPIRequest $request)
    {
        $user=Auth::user();
         
        $input = $request->all();

        if($user->getTable() == 'doctors')
         {
            $input['doctor_id']=$user->id;
            $input['from']='doctor';
         }
        else if($user->getTable() == 'organizations')
         {
            $input['org_id']=$user->id;
            $input['from']='org';
         }

        $relation = DoctorOrg::where('doctor_id',$input['doctor_id'])->where('org_id',$input['org_id'])->first();
        if($relation)
        return $this->sendError('You have a pending request already');

        $doctorOrg = $this->doctorOrgRepository->create($input);
        if($input['from']=='doctor')
        {
            $organization = $doctorOrg->organization;
            $doctor = $doctorOrg->doctor;
            
            $this->sendNotification($organization->id,"NEW REQUEST",1,$organization->device_token,'you have got a new work request from '.$doctor->name,$doctor->picture,0,0);
            unset($doctorOrg['organization']);
            unset($doctorOrg['doctor']);

        }

        if($input['from']=='org')
        {
            $doctor = $doctorOrg->doctor;
            $organization = $doctorOrg->organization;
            
            $this->sendNotification($doctor->id,"NEW REQUEST",1,$doctor->device_token,'you have got a new work request from '.$organization->name,$organization->image,0,0);
            unset($doctorOrg['doctor']);
            unset($doctorOrg['organization']);

        }

        return $this->sendResponse($doctorOrg->toArray(), 'Doctor Org saved successfully');
    }


    public function getPending()
    {
        $user=Auth::user();
        if($user->getTable() == 'doctors')
        $doctor_org=DoctorOrg::where('doctor_id',$user->id)->where('status','PENDING')->where('from','org')->get();
        else if($user->getTable() == 'organizations')
        $doctor_org=DoctorOrg::where('org_id',$user->id)->where('status','PENDING')->where('from','doctor')->get();
        return Response::json(['relations'=> DoctorOrgResource::collection($doctor_org)]);
        
    }

    public function accept(Request $request)
    {
        $validated = $request->validate([
            'id' => 'required|exists:doctor_orgs',
            ]);
         $doctor_org=DoctorOrg::where('id',$request->id)->first();      
         $doctor_org->update(['status'=>'ACCEPTED']);

         if($doctor_org['from']=='org')
         {
             $organization = $doctor_org->organization;
             $doctor = $doctor_org->doctor;

             $this->sendNotification($organization->id,"REQUEST ACCEPTED",0,$organization->device_token,'your work request with '.$doctor->name.' is accepted',$doctor->picture,0,0);
            //  unset($doctor_org['organization']);
         }
 
         if($doctor_org['from']=='doctor')
         {
             $doctor = $doctor_org->doctor;
             $organization = $doctor_org->organization;
             
             $this->sendNotification($doctor->id,"REQUEST ACCEPTED",0,$doctor->device_token,'your work request with '.$organization->name.' is accepted',$organization->picture,0,0);
            //  unset($doctorOrg['doctor']);
         }

        return $this->sendSuccess('Request accepted');
    }

    public function reject(Request $request)
    {
        $validated = $request->validate([
            'id' => 'required|exists:doctor_orgs',
            ]);
         $doctor_org=DoctorOrg::where('id',$request->id)->delete();      
        return $this->sendSuccess('Request rejected');
    }

    public function getDoctors(Request $request)

    {
        $validated = $request->validate([
            'org_id' => 'required|exists:organizations,id',
            ]);
         $doctor_org=DoctorOrg::where('org_id',$request->org_id)->where('status','ACCEPTED')->get();      
         return Response::json(['relations'=> DoctorOrgResource::collection($doctor_org)]);
    }

    public function getOrganizations(Request $request)

    {
        $validated = $request->validate([
            'doctor_id' => 'required|exists:doctors,id',
            ]);
         $doctor_org=DoctorOrg::where('doctor_id',$request->doctor_id)->where('status','ACCEPTED')->get();      
         return Response::json(['relations'=> DoctorOrgResource::collection($doctor_org)]);
    }

    

    /**
     * Display the specified DoctorOrg.
     * GET|HEAD /doctorOrgs/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DoctorOrg $doctorOrg */
        $doctorOrg = $this->doctorOrgRepository->find($id);

        if (empty($doctorOrg)) {
            return $this->sendError('Doctor Org not found');
        }

        return $this->sendResponse($doctorOrg->toArray(), 'Doctor Org retrieved successfully');
    }

    /**
     * Update the specified DoctorOrg in storage.
     * PUT/PATCH /doctorOrgs/{id}
     *
     * @param int $id
     * @param UpdateDoctorOrgAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDoctorOrgAPIRequest $request)
    {
        $input = $request->all();

        /** @var DoctorOrg $doctorOrg */
        $doctorOrg = $this->doctorOrgRepository->find($id);

        if (empty($doctorOrg)) {
            return $this->sendError('Doctor Org not found');
        }

        $doctorOrg = $this->doctorOrgRepository->update($input, $id);

        return $this->sendResponse($doctorOrg->toArray(), 'DoctorOrg updated successfully');
    }

    /**
     * Remove the specified DoctorOrg from storage.
     * DELETE /doctorOrgs/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DoctorOrg $doctorOrg */
        $doctorOrg = $this->doctorOrgRepository->find($id);

        if (empty($doctorOrg)) {
            return $this->sendError('Doctor Org not found');
        }

        $doctorOrg->delete();

        return $this->sendSuccess('Doctor Org deleted successfully');
    }
}
