<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateHistoryAPIRequest;
use App\Http\Requests\API\UpdateHistoryAPIRequest;
use App\Models\History;
use App\Models\Doctor;
use App\Repositories\HistoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Carbon\Carbon;
use App\Http\Resources\History\HistoryResource;
use App\Traits\NotificationTrait;

use Illuminate\Support\Facades\Crypt;


/**
 * Class HistoryController
 * @package App\Http\Controllers\API
 */

class HistoryAPIController extends AppBaseController
{
    use NotificationTrait;

    /** @var  HistoryRepository */
    private $historyRepository;

    public function __construct(HistoryRepository $historyRepo)
    {
        $this->historyRepository = $historyRepo;
    }

    /**
     * Display a listing of the History.
     * GET|HEAD /histories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $histories = $this->historyRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($histories->toArray(), 'Histories retrieved successfully');
    }

    /**
     * Store a newly created History in storage.
     * POST /histories
     *
     * @param CreateHistoryAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateHistoryAPIRequest $request)
    {
        $input = $request->all();

        $input['date']=Carbon::now()->format('d/m/Y');
       
        foreach($input['prescription'] as $prescription){
            $json[]=$prescription;
            }
            $input['prescription'] = json_encode($json,JSON_UNESCAPED_UNICODE);
            $input['description'] =Crypt::encryptString($request->description);
            $input['prescription'] =Crypt::encryptString($input['prescription']);
            $input['chronic'] =Crypt::encryptString($request->chronic);

        $history = $this->historyRepository->create($input);
        $patient=$history->patient;
        $doctor = Doctor::where('id',$request->doctor_id)->first();
        // return $doctor;
        $this->sendNotification($patient->id,"Your visit has ended",6,$patient->device_token,'please rate your doctor',$doctor->picture,$doctor->id,$doctor->name);

        return Response::json(['history'=>HistoryResource::make($history)]);

        // $history->fill([
        //         'description' => Crypt::encryptString($request->description),
        //     ])->save();
        return $this->sendResponse($history->toArray(), 'History saved successfully');
    }


    /**
     * Display the specified History.
     * GET|HEAD /histories/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var History $history */
        $history = $this->historyRepository->find($id);

        if (empty($history)) {
            return $this->sendError('History not found');
        }

        return $this->sendResponse($history->toArray(), 'History retrieved successfully');
    }

    /**
     * Update the specified History in storage.
     * PUT/PATCH /histories/{id}
     *
     * @param int $id
     * @param UpdateHistoryAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHistoryAPIRequest $request)
    {
        $input = $request->all();

        /** @var History $history */
        $history = $this->historyRepository->find($id);

        if (empty($history)) {
            return $this->sendError('History not found');
        }

        $history = $this->historyRepository->update($input, $id);

        return $this->sendResponse($history->toArray(), 'History updated successfully');
    }

    /**
     * Remove the specified History from storage.
     * DELETE /histories/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var History $history */
        $history = $this->historyRepository->find($id);

        if (empty($history)) {
            return $this->sendError('History not found');
        }

        $history->delete();

        return $this->sendSuccess('History deleted successfully');
    }
}
