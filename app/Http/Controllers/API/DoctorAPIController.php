<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDoctorAPIRequest;
use App\Http\Requests\API\UpdateDoctorAPIRequest;
use App\Models\Doctor;
use App\Models\Rating;
use App\Models\Patient;
use App\Models\Operation;
use App\Models\History;
use App\Models\Family;  
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use App\Repositories\DoctorRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Http\Resources\Doctor\DoctorResource;
use App\Http\Resources\History\HistoryPatientResource;
use App\Http\Resources\Doctor\DoctorHistoryResource;
use App\Http\Resources\Doctor\DoctorReviewsResource;
use App\Http\Resources\Major\MajorFilterResource;
use App\Http\Resources\Family\RequestResource;

use App\Http\Resources\Doctor\DoctorUpdateResource;
use App\Http\Resources\Operation\OperationResource;


use App\Http\Resources\Rating\RatingResource;

use Illuminate\Support\Facades\Auth;    


/**
 * Class DoctorController
 * @package App\Http\Controllers\API
 */

class DoctorAPIController extends AppBaseController
{
    /** @var  DoctorRepository */
    private $doctorRepository;

    public function __construct(DoctorRepository $doctorRepo)
    {
        $this->doctorRepository = $doctorRepo;
    }

    /**
     * Display a listing of the Doctor.
     * GET|HEAD /doctors
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $doctors = $patient->doctorRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $patient->sendResponse($doctors->toArray(), 'Doctors retrieved successfully');
    }

    /**
     * Store a newly created Doctor in storage.
     * POST /doctors
     *
     * @param CreateDoctorAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDoctorAPIRequest $request)
    {
        $input = $request->all();
        if($request->hasFile('certificate')){
            $name = time(). "." .$request->file('certificate')->getClientOriginalName();
            $request->certificate->move(public_path()."/images/doctor/" , $name);
            $input['certificate'] = url(asset('public/images/doctor/'.$name));         
            }  


            if($request->hasFile('picture')){
                $name = time(). "." .$request->file('picture')->getClientOriginalName();
                $request->picture->move(public_path()."/images/doctor/" , $name);
                $input['picture'] = url(asset('public/images/doctor/'.$name));         
                }  
                $input['user_type']=2;

        $doctor = $this->doctorRepository->create($input);
        $doctor->token = $doctor->createToken('doctor')->accessToken;
        return Response::json(['user_type'=>"doctor",'user'=> new DoctorResource($doctor)]);

        return $patient->sendResponse($doctor->toArray(), 'Doctor saved successfully');
    }
    public function login(Request $req)
    {
        $validated = $req->validate([
            'email'=>'required|email',
            'password'=>'required|min:6',
            'device_token'=>'required'
        ]);
        if ( Auth::guard('doctor')->attempt(['email' => $req->email  , 'password' => $req->password] )) {
            $doctor = auth()->guard('doctor')->user();
            
            $doctor->update(['device_token'=>$req->device_token]);
            $doctor->token = $doctor->createToken('doctor')->accessToken;
          return Response::json(['user_type'=>"doctor",'user'=> new DoctorResource($doctor)]);
    
        } 
        else{
        return Response::json(['message'=>"incorrect"]);
        }
    }

    public function checkPassword(Request $req)
    {
        $validated = $req->validate([
            'password'=>'required',
        ]);
        $doctor=Auth::user();
        if ( Auth::guard('doctor')->attempt(['email' => $doctor->email  , 'password' => $req->password] )) {
          return Response::json(['user_type'=>"doctor",'message'=> 'correct password']);
        }
        return Response::json(['user_type'=>"doctor",'message'=> 'incorrect password']);

    }

    public function getReviews()
    {
        $id=Auth::id();
        $ratings=Rating::where('doctor_id',$id)->cursorPaginate(8);
        $next_page_url=$ratings->nextPageUrl()?$ratings->nextPageUrl():"";
        $prev_page_url=$ratings->previousPageUrl()?$ratings->previousPageUrl():"";
        return Response::json(['ratings'=> RatingResource::collection($ratings), 'next_page_url'=>$next_page_url , 'prev_page_url'=>$prev_page_url]);
        
    }

    public function orgGetDoctorReviews(Request $request)
    {
        $validated = $request->validate([
            'doctor_id' => 'required|exists:doctors,id',
            ]);
        $ratings=Rating::where('doctor_id',$request->doctor_id)->get();
        return Response::json(['reviews'=> RatingResource::collection($ratings)]);

        
    }
    public function topRated()
    {
        $doctors=Doctor::orderBy('rate', 'desc')->get();
        // $next_page_url=$doctors->nextPageUrl()?$doctors->nextPageUrl():"";
        // $prev_page_url=$doctors->previousPageUrl()?$doctors->previousPageUrl():"";
        return Response::json(['doctors'=> DoctorReviewsResource::collection($doctors)]);
   
    }

    public function details()
    {
        $doctor=Auth::user();
        // $doctor->load('ratings');
        // $rate=$doctor->ratings->avg('rate');

        return Response::json(['user_type'=>"doctor",'user'=> new DoctorUpdateResource($doctor)]);
        
    }

    public function history()
    {
        $user=Auth::user();
        $histories=History::where('doctor_id',$user->id)->orderBy('patient_id')->get();
       $histories=$histories->unique(['patient_id']);
       if ($histories)
        $histories->load('patient');
     
        foreach($histories as $history)
         $patients[]=Patient::where('id',$history->patient_id)->get();
        //  return $patients;
        //  $patients[] = array_search($user->id,$patients);
        
        foreach($patients as $patient1)
        {
            $patient1->load('history');
            $patient1->where('doctor_id',$user->id);
        // return Response::json(['user'=> $patient]);
        foreach($patient1 as $patient)
       {     // return $p->qr;
        {
            // return $patient->history;

        //    $patients[]= new PatientHistoryResource($patient);
            if(!$patient->qr)
            $patient->qr ="";
            if($patient->cronic){
                $patient->chronic=json_decode($patient->cronic);
                unset($patient->cronic);
            }
            else 
            {
                unset($patient->cronic);
                $patient->chronic=[];


            }
            if(!$patient->picture)
            $patient->picture ="";
            if(!$patient->emergency1)
            $patient->emergency1 =""; 
              if(!$patient->emergency2)
            $patient->emergency2 ="";   
            if(!$patient->emergency3)
            $patient->emergency3 ="";
            if(!$patient->device_token)
            $patient->device_token ="";
            
            //  $patient->histories = $patient->history->where('doctor_id',$user->id);

            // // //  dd($patient);
            //  unset($patient['history']);
            //  $patient->history = $patient['histories'];
            //  unset($patient['histories']);
             
           foreach($patient->history as $history)
            // return $history->description;
        //    foreach($history1 as $history)

            {
                // echo $history->id;
                $history->description=Crypt::decryptString($history->description);
                $history->prescriptions=json_decode(Crypt::decryptString($history->prescription));
                unset($history->prescription);

                 $history->chronic=Crypt::decryptString($history->chronic);

            }
            
            
        }   

            }

        }
        
            // return $cronic;
        return Response::json(['user'=> $patients]);

    }

    public function getAll()
    {
        $doctors=Doctor::get();
        return Response::json(['doctors'=> DoctorUpdateResource::collection($doctors)]);

    }

    public function getPendingOperation()
    {
        $id=Auth::id();
        $operations=Operation::where('doctor1_id',$id)->where('status','PENDING')->get();
        return Response::json(['operations'=> OperationResource::collection($operations)]);

    }

    public function getFilteredMajor(Request $request)
    {
        $validated = $request->validate([
            'qr'=>'required|exists:patients,qr',
        ]);
        $filtered1=[];
        $filtered2=[];
        $filtered3=[];
        $same_major=[];

        $user=Auth::user();
        $patient=Patient::where('qr',$request->qr)->first();
        $histories=History::where('patient_id',$patient->id)->with('doctor')->orderBy('date', 'DESC')->get();
        $same_major=$histories->where('doctor.major',$user->major);
        // return $user;
        if($user->major == "Internal Medicine.\r\nطب الباطنة")
        {
            
         $filtered1=$histories->where('doctor.major','Hepatology.\r\nطب امراض الكبد');
         $filtered2=$histories->where('doctor.major','Hematology.\r\nامراض الدم');
        $filtered = $filtered1->merge($filtered2);    
        }

        if($user->major == "Cardiology.\r\nالقلب والاوعية الدموية"  || $user->major == "Obstetrics and Gynecology.\r\nنساء وتوليد" )
        {
         $filtered1=$histories->where('doctor.major','Anesthesiology & Recovery\r\nطب التخدير والانعاش');
         $filtered2=$histories->where('doctor.major','Oncology.\r\nطب الاورام');
        $filtered = $filtered1->merge($filtered2);    
        }


        if($user->major == "Gastroenterology.\r\nباطنة جهاز هضمي ومناظير")
        {
         $filtered1=$histories->where('doctor.major','Pediatrics.\r\nطب اطفال');
         $filtered2=$histories->where('doctor.major','Allergy and immunology.\r\nالحساسية والمناعة');
        $filtered = $filtered1->merge($filtered2);    
        }


        if($user->major == "Ophthalmology.\r\nطب العيون")
        {
         $filtered1=$histories->where('doctor.major','Neurology.\r\nطب الاعصاب');
         $filtered2=$histories->where('doctor.major','Ear, nose and throat (ENT)\r\nطب الانف والاذن والحنجرة');
        $filtered = $filtered1->merge($filtered2);    
        }


        if($user->major == "Pediatrics.\r\nطب اطفال")
        {
         $filtered1=$histories->where('doctor.major','Gastroenterology.\r\nباطنة جهاز هضمي ومناظير');
         $filtered2=$histories->where('doctor.major','Allergy and immunology.\r\nالحساسية والمناعة');
        $filtered = $filtered1->merge($filtered2);    
        }


        if($user->major == "Ear, nose and throat (ENT)\r\nطب الانف والاذن والحنجرة" || $user->major == "Neurology.\r\nطب الاعصاب")
        {
         $filtered1=$histories->where('doctor.major','Ophthalmology.\r\nطب العيون');
        }

        if($user->major == "Oncology.\r\nطب الاورام")
        {
         $filtered1=$histories->where('doctor.major','Internal Medicine.\r\nطب الباطنة');
         $filtered2=$histories->where('doctor.major','Cardiology.\r\nالقلب والاوعية الدموية');
         $filtered3=$histories->where('doctor.major','Obstetrics and Gynecology.\r\nنساء وتوليد');
        $filtered4 = $filtered1->merge($filtered2);   
        $filtered = $filtered3->merge($filtered4);    
        }


        
        if($user->major == "Dermatology.\r\nجلدية" )
        {
         $filtered1=$histories->where('doctor.major','Oncology.\r\nطب الاورام');
        }



        if($user->major == "Hematology.\r\nامراض الدم"  ||  $user->major == "Urology.\r\nالمسالك البولية")
        {
         $filtered1=$histories->where('doctor.major','Oncology.\r\nطب الاورام');
         $filtered2=$histories->where('doctor.major','Internal Medicine.\r\nطب الباطنة');
        $filtered = $filtered1->merge($filtered2);    
        }

        if($user->major == "Nephrology.\r\nطب امراض الكلى")
        {
         $filtered1=$histories->where('doctor.major','Hematology.\r\nامراض الدم');
         $filtered2=$histories->where('doctor.major','Internal Medicine.\r\nطب الباطنة');
        $filtered = $filtered1->merge($filtered2);    
        }


        if($user->major == "Hepatology.\r\nطب امراض الكبد")
        {
         $filtered=$histories->where('doctor.major','Internal Medicine.\r\nطب الباطنة');
        }

        if($user->major == "Allergy and immunology.\r\nالحساسية والمناعة")
        {
         $filtered1=$histories->where('doctor.major','Gastroenterology.\r\nباطنة جهاز هضمي ومناظير');
         $filtered2=$histories->where('doctor.major','Pediatrics.\r\nطب اطفال');
        $filtered = $filtered1->merge($filtered2);    
        }

        
        if($user->major == "Anesthesiology & Recovery\r\nطب التخدير والانعاش")
        {
         $filtered=$histories->where('Obstetrics and Gynecology.\r\nنساء وتوليد');
        }
    //     $family=Family::where('user_id1',$patient->id)->where('status','ACCEPTED')->get();
        
    //     $family2=Family::where('user_id2',$patient->id)->where('status','ACCEPTED')->get();
        
    //    $family = $family->merge($family2);    
    // //    return $family;
    //     return $patient;
    //        return $families = $patient->family;
    //     foreach ($families as $family1)
    //    return $family1->where('family1.status','ACCEPTED');
        // return $family;
        // $operations=Operation::where('patient_id',$patient->id)->where('status','ACCEPTED')->get();
        

    // $histories=$histories->orderBy('date');
        
        return Response::json(['all'=> MajorFilterResource::collection($histories) ,'same_major'=> MajorFilterResource::collection($same_major),'filter1'=> MajorFilterResource::collection($filtered1) , 'filter2'=> MajorFilterResource::collection($filtered2) , 'filter3'=> MajorFilterResource::collection($filtered3)]);
    }


    public function filterMajor(Request $request)
    {
        $user=Auth::user();
        $filtered[]='ALL';
        $filtered[]=$user->major;
        if($user->major == "Internal Medicine.\r\nطب الباطنة")
        {
        $filtered[] = "Hepatology.\r\nطب امراض الكبد"; 
        $filtered[] = "Hematology.\r\nامراض الدم";    

        }

        if($user->major == "Cardiology.\r\nالقلب والاوعية الدموية"  || $user->major == "Obstetrics and Gynecology.\r\nنساء وتوليد" )
        {
         $filtered[] = "Anesthesiology & Recovery\r\nطب التخدير والانعاش"; 
         $filtered[] = "Oncology.\r\nطب الاورام";    
 
        }


        if($user->major == "Gastroenterology.\r\nباطنة جهاز هضمي ومناظير")
        {
         $filtered[] = "Pediatrics.\r\nطب اطفال"; 
         $filtered[] = "Allergy and immunology.\r\nالحساسية والمناعة";    
    
        }


        if($user->major == "Ophthalmology.\r\nطب العيون")
        {
         $filtered[] = "Neurology.\r\nطب الاعصاب"; 
         $filtered[] = "Ear, nose and throat (ENT)\r\nطب الانف والاذن والحنجرة";    
        }


        if($user->major == "Pediatrics.\r\nطب اطفال")
        {
         $filtered[] = "Gastroenterology.\r\nباطنة جهاز هضمي ومناظير"; 
         $filtered[] = "Allergy and immunology.\r\nالحساسية والمناعة";    
      }


        if($user->major == "Ear, nose and throat (ENT)\r\nطب الانف والاذن والحنجرة" || $user->major == "Oncology.\r\nطب الاورام")
        {
         $filtered[] = "Ophthalmology.\r\nطب العيون"; 

        }

        if($user->major == "Oncology.\r\nطب الاورام")
        {
         $filtered[] = "Internal Medicine.\r\nطب الباطنة"; 
        $filtered[] = "Cardiology.\r\nالقلب والاوعية الدموية";    
        $filtered[] = "Obstetrics and Gynecology.\r\nنساء وتوليد";    
        }


        
        if($user->major == "Dermatology.\r\nجلدية" )
        {
         $filtered[]="Oncology.\r\nطب الاورام";

        }



        if($user->major == "Hematology.\r\nامراض الدم"  ||  $user->major == "Urology.\r\nالمسالك البولية")
        {
         $filtered[] = "Oncology.\r\nطب الاورام";    
         $filtered[] = "Internal Medicine.\r\nطب الباطنة";    

        }

        if($user->major == "Nephrology.\r\nطب امراض الكلى")
        {
         $filtered[] = "Hematology.\r\nامراض الدم"; 
         $filtered[] = "Internal Medicine.\r\nطب الباطنة";    
   
        }


        if($user->major == "Hepatology.\r\nطب امراض الكبد")
        {
         $filtered[]="Internal Medicine.\r\nطب الباطنة";
        }

        if($user->major == "Allergy and immunology.\r\nالحساسية والمناعة")
        {
        $filtered[] = "Gastroenterology.\r\nباطنة جهاز هضمي ومناظير"; 
        $filtered[] = "Pediatrics.\r\nطب اطفال";    
       
        }

        
        if($user->major == "Anesthesiology & Recovery\r\nطب التخدير والانعاش")
        {
         $filtered[]="Obstetrics and Gynecology.\r\nنساء وتوليد";
        }

        $filtered[] = 'chronic';    
        // $filtered[] = 'family';    

        
        return Response::json(['filter'=> $filtered]);
    }
 
    /**
     * Display the specified Doctor.
     * GET|HEAD /doctors/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Doctor $doctor */
        $doctor = $patient->doctorRepository->find($id);

        if (empty($doctor)) {
            return $patient->sendError('Doctor not found');
        }

        return $patient->sendResponse($doctor->toArray(), 'Doctor retrieved successfully');
    }

    /**
     * Update the specified Doctor in storage.
     * PUT/PATCH /doctors/{id}
     *
     * @param int $id
     * @param UpdateDoctorAPIRequest $request
     *
     * @return Response
     */
    public function update(UpdateDoctorAPIRequest $request)
    {
        $input = $request->all();
        $id=Auth::id();

        /** @var Doctor $doctor */
        $doctor = $this->doctorRepository->find($id);
        if($request->hasFile('picture')){
            $name = time(). "." .$request->file('picture')->getClientOriginalName();
            $request->picture->move(public_path()."/images/doctor/" , $name);
            $input['picture'] = url(asset('public/images/doctor/'.$name)); 
                    
            } 

        $doctor = $this->doctorRepository->update($input, $id);
        return Response::json(['user_type'=>"doctor",'user'=> new DoctorUpdateResource($doctor)]);

        // return $patient->sendResponse($doctor->toArray(), 'Doctor updated successfully');
    }

    /**
     * Remove the specified Doctor from storage.
     * DELETE /doctors/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Doctor $doctor */
        $doctor = $patient->doctorRepository->find($id);

        if (empty($doctor)) {
            return $patient->sendError('Doctor not found');
        }

        $doctor->delete();

        return $patient->sendSuccess('Doctor deleted successfully');
    }
}
