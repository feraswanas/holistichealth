<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOrganizationAPIRequest;
use App\Http\Requests\API\UpdateOrganizationAPIRequest;
use App\Models\Organization;
use App\Models\Operation;
use App\Http\Resources\Operation\OperationResource;

use App\Repositories\OrganizationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Support\Facades\Auth;    
use App\Http\Resources\Organization\OrganizationResource;
use App\Http\Resources\Organization\NearByOrganizationResource;
use App\Http\Resources\Organization\OrganizationUpdateResource;
use DB;

/**
 * Class OrganizationController
 * @package App\Http\Controllers\API
 */

class OrganizationAPIController extends AppBaseController
{
    /** @var  OrganizationRepository */
    private $organizationRepository;

    public function __construct(OrganizationRepository $organizationRepo)
    {
        $this->organizationRepository = $organizationRepo;
    }

    /**
     * Display a listing of the Organization.
     * GET|HEAD /organizations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $organizations = $this->organizationRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($organizations->toArray(), 'Organizations retrieved successfully');
    }

    /**
     * Store a newly created Organization in storage.
     * POST /organizations
     *
     * @param CreateOrganizationAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateOrganizationAPIRequest $request)
    {
        $input = $request->all();
        if($request->hasFile('image')){
            $name = time(). "." .$request->file('image')->getClientOriginalName();
            $request->image->move(public_path()."/images/organization/" , $name);
            $input['image'] = url(asset('public/images/organization/'.$name));         
            }  

            if($request->hasFile('tax')){
                $name = time(). "." .$request->file('tax')->getClientOriginalName();
                $request->tax->move(public_path()."/images/organization/" , $name);
                $input['tax'] = url(asset('public/images/organization/'.$name));         
                }  

            if(!$input['phone2'])
            $input['phone2']="0";
            
            if($input['major'] != [null])
            {
                foreach($input['major'] as $major){
                $json[]=$major;
                }
            $input['major'] = json_encode($json,JSON_UNESCAPED_UNICODE);
            }
            else 
            $input['major'] = null;

            $input['user_type']=3;
           
        $organization = $this->organizationRepository->create($input);
        // return $input;
        // $organization=Organization::create($input);
        $organization->token = $organization->createToken('organization')->accessToken;
        return Response::json(['user_type'=>"org",'user'=> new OrganizationResource($organization)]);

        return $this->sendResponse($organization->toArray(), 'Organization saved successfully');
    }


    public function login(Request $req)
    {
        $validated = $req->validate([
            'email'=>'required|email',
            'password'=>'required|min:6',
            'device_token'=>'required'
        ]);
        if ( Auth::guard('organization')->attempt(['email' => $req->email  , 'password' => $req->password] )) {
            
            $organization = auth()->guard('organization')->user();
            
            $organization->update(['device_token'=>$req->device_token]);
            $organization->token = $organization->createToken('organization')->accessToken;
            return Response::json(['user_type'=>"org",'user'=> new OrganizationResource($organization)]);
        } 
        else{
        return Response::json(['message'=>"incorrect"]);
        }
    }

    public function checkPassword(Request $req)
    {
        $validated = $req->validate([
            'password'=>'required',
        ]);
        $organization=Auth::user();
        if ( Auth::guard('organization')->attempt(['email' => $organization->email  , 'password' => $req->password] )) {
          return Response::json(['user_type'=>"organization",'message'=> 'correct password']);
        }
        return Response::json(['user_type'=>"organization",'message'=> 'incorrect password']);

    }


    public function details()
    {
        $organization=Auth::user();
        return Response::json(['user_type'=>"org",'user'=> new OrganizationUpdateResource($organization)]);
        
    }

    public function getAll()
    {
        $organizations=Organization::get();
        return Response::json(['organizations'=> OrganizationUpdateResource::collection($organizations)]);

    }

    public function nearBy(Request $request)
    {
        $validated = $request->validate([
            'lat'=>'required',
            'long'=>'required',
            
        ]);
   $organizations = DB::table("organizations")
            ->select("*"
                ,DB::raw("6371 * acos(cos(radians(" . $request->lat . ")) 
                * cos(radians(organizations.lat)) 
                * cos(radians(organizations.long) - radians(" . $request->long . ")) 
                + sin(radians(" .$request->lat. ")) 
                * sin(radians(organizations.lat))) AS distance"))
                ->orderBy('distance')
                ->get();
                return Response::json(['organizations'=> NearByOrganizationResource::collection($organizations)]);
    }

    public function getOperations()
    {
        $id=Auth::id();
        $operations=Operation::where('organization_id',$id)->get();
        return Response::json(['operations'=> OperationResource::collection($operations)]);

    }


    /**
     * Display the specified Organization.
     * GET|HEAD /organizations/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Organization $organization */
        $organization = $this->organizationRepository->find($id);

        if (empty($organization)) {
            return $this->sendError('Organization not found');
        }

        return $this->sendResponse($organization->toArray(), 'Organization retrieved successfully');
    }

    /**
     * Update the specified Organization in storage.
     * PUT/PATCH /organizations/{id}
     *
     * @param int $id
     * @param UpdateOrganizationAPIRequest $request
     *
     * @return Response
     */
    public function update(UpdateOrganizationAPIRequest $request)
    {
        $input = $request->all();

        $id=Auth::id();
        if($request->hasFile('image')){
            
            $name = time(). "." .$request->file('image')->getClientOriginalName();
            $request->image->move(public_path()."/images/organization/" , $name);
            $input['image'] = url(asset('public/images/organization/'.$name));         
            }  
            /** @var Organization $organization */
        $organization = $this->organizationRepository->find($id);

        if (empty($organization)) {
            return $this->sendError('Organization not found');
        }
        
        if(!$input['phone2'])
        $input['phone2']="0";
        

        $organization = $this->organizationRepository->update($input, $id);
        return Response::json(['user_type'=>"org",'user'=> new OrganizationUpdateResource($organization)]);

        return $this->sendResponse($organization->toArray(), 'Organization updated successfully');
    }

    /**
     * Remove the specified Organization from storage.
     * DELETE /organizations/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Organization $organization */
        $organization = $this->organizationRepository->find($id);

        if (empty($organization)) {
            return $this->sendError('Organization not found');
        }

        $organization->delete();

        return $this->sendSuccess('Organization deleted successfully');
    }
}
