<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOperationAPIRequest;
use App\Http\Requests\API\UpdateOperationAPIRequest;
use App\Models\Operation;
use App\Models\Doctor;
use App\Traits\NotificationTrait;

use App\Repositories\OperationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Support\Facades\Auth;    

/**
 * Class OperationController
 * @package App\Http\Controllers\API
 */

class OperationAPIController extends AppBaseController
{
    use NotificationTrait;
    
    /** @var  OperationRepository */
    private $operationRepository;

    public function __construct(OperationRepository $operationRepo)
    {
        $this->operationRepository = $operationRepo;
    }

    /**
     * Display a listing of the Operation.
     * GET|HEAD /operations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $operations = $this->operationRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($operations->toArray(), 'Operations retrieved successfully');
    }

    /**
     * Store a newly created Operation in storage.
     * POST /operations
     *
     * @param CreateOperationAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateOperationAPIRequest $request)
    {
        $input = $request->all();
        $input['organization_id']=Auth::id();

        $operation = $this->operationRepository->create($input);
        // $user=Doctor::where('id',$request->doctor1_id)->first();
        $operation->load('doctor1','patient','organization');
        
        // return $user;
        $this->sendNotification($operation->doctor1->id,"NEW OPERATION",2,$operation->doctor1->device_token,'you have got a new operation request to '.$operation->patient->name.' in '.$operation->organization->name,$operation->organization->image,0,0);

        return $this->sendResponse($operation->toArray(), 'Operation saved successfully');
    }

    public function accept(Request $request)
    {
        $validated = $request->validate([
            'id' => 'required|exists:operations',
            ]);
        $operation=Operation::where('id',$request->id)->first();
        $operation->update(['status'=>'ACCEPTED']);
        $operation->load('organization','doctor1');
        $this->sendNotification($operation->organization->id,"OPERATION ACCEPTED",0,$operation->organization->device_token,'your operation request is accepted with '.$operation->doctor1->name,$operation->doctor1->picture,0,0);

        return $this->sendSuccess('operation accepted successfully');

    }

    public function reject(Request $request)
    {
        $validated = $request->validate([
            'id' => 'required|exists:operations',
            ]);
            $operation=Operation::where('id',$request->id)->first();
            $operation->update(['status'=>'REJECTED']);
            $operation->load('organization','doctor1');
    
            $this->sendNotification($operation->organization->id,"OPERATION REJECTED",0,$operation->organization->device_token,'your operation request is rejected with '.$operation->doctor1->name,$operation->doctor1->picture,0,0);
    
        return $this->sendSuccess('operation rejected successfully');

    }

    /**
     * Display the specified Operation.
     * GET|HEAD /operations/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Operation $operation */
        $operation = $this->operationRepository->find($id);

        if (empty($operation)) {
            return $this->sendError('Operation not found');
        }

        return $this->sendResponse($operation->toArray(), 'Operation retrieved successfully');
    }

    /**
     * Update the specified Operation in storage.
     * PUT/PATCH /operations/{id}
     *
     * @param int $id
     * @param UpdateOperationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOperationAPIRequest $request)
    {
        $input = $request->all();

        /** @var Operation $operation */
        $operation = $this->operationRepository->find($id);

        if (empty($operation)) {
            return $this->sendError('Operation not found');
        }

        $operation = $this->operationRepository->update($input, $id);

        return $this->sendResponse($operation->toArray(), 'Operation updated successfully');
    }

    /**
     * Remove the specified Operation from storage.
     * DELETE /operations/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Operation $operation */
        $operation = $this->operationRepository->find($id);

        if (empty($operation)) {
            return $this->sendError('Operation not found');
        }

        $operation->delete();

        return $this->sendSuccess('Operation deleted successfully');
    }
}
