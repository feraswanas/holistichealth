<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMajorAPIRequest;
use App\Http\Requests\API\UpdateMajorAPIRequest;
use App\Models\Major;
use App\Repositories\MajorRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MajorController
 * @package App\Http\Controllers\API
 */

class MajorAPIController extends AppBaseController
{
    /** @var  MajorRepository */
    private $majorRepository;

    public function __construct(MajorRepository $majorRepo)
    {
        $this->majorRepository = $majorRepo;
    }

    /**
     * Display a listing of the Major.
     * GET|HEAD /majors
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $majors = Major::whereNotNull('major')->pluck('major');
        return Response::json(['major'=> $majors]);

        // return $this->sendResponse($majors->toArray(), 'Majors retrieved successfully');
    }

    public function chronic(Request $request)
    {
        $chronic = Major::whereNotNull('chronic')->pluck('chronic');
        return Response::json(['chronic'=> $chronic]);

        // return $this->sendResponse($majors->toArray(), 'Majors retrieved successfully');
    }

    public function getDrugs()
    {
        $drugs = Major::whereNotNull('drug')->pluck('drug');
        return Response::json(['drugs'=> $drugs]);

        // return $this->sendResponse($majors->toArray(), 'Majors retrieved successfully');
    }
    public function addDrug(Request $request)
    {
        $major= new Major;
        $major->drug=$request->drug;
        $major->save();
        // Major::create($request);
        return Response::json(['message'=> "added"]);

        // return $this->sendResponse($majors->toArray(), 'Majors retrieved successfully');
    }
    /**
     * Store a newly created Major in storage.
     * POST /majors
     *
     * @param CreateMajorAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMajorAPIRequest $request)
    {
        $input = $request->all();

        $major = $this->majorRepository->create($input);

        return $this->sendResponse($major->toArray(), 'Major saved successfully');
    }

    /**
     * Display the specified Major.
     * GET|HEAD /majors/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Major $major */
        $major = $this->majorRepository->find($id);

        if (empty($major)) {
            return $this->sendError('Major not found');
        }

        return $this->sendResponse($major->toArray(), 'Major retrieved successfully');
    }

    /**
     * Update the specified Major in storage.
     * PUT/PATCH /majors/{id}
     *
     * @param int $id
     * @param UpdateMajorAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMajorAPIRequest $request)
    {
        $input = $request->all();

        /** @var Major $major */
        $major = $this->majorRepository->find($id);

        if (empty($major)) {
            return $this->sendError('Major not found');
        }

        $major = $this->majorRepository->update($input, $id);

        return $this->sendResponse($major->toArray(), 'Major updated successfully');
    }

    /**
     * Remove the specified Major from storage.
     * DELETE /majors/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Major $major */
        $major = $this->majorRepository->find($id);

        if (empty($major)) {
            return $this->sendError('Major not found');
        }

        $major->delete();

        return $this->sendSuccess('Major deleted successfully');
    }
}
