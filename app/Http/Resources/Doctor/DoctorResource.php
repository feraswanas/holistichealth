<?php

namespace App\Http\Resources\Doctor;

use Illuminate\Http\Resources\Json\JsonResource;

class DoctorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return[
            'id' => $this->id,   
            'name' => $this->name?$this->name:"",   
            'birth_date' => $this->birth_date?$this->birth_date:"",
            'gender' => $this->gender?$this->gender:"",   
            'major' => $this->major?$this->major:"",
            'email' => $this->email?$this->email:"",
            'rate' => $this->rate?$this->rate:0.1,
            'certificate' => $this->certificate?$this->certificate:"",
            'phone' => $this->phone?$this->phone:"",
            'id_number' => $this->id_number?$this->id_number:"",
            'picture' => $this->picture?$this->picture:"",
            'user_type' => $this->user_type?$this->user_type:"",
            'description' => $this->description?$this->description:"",
            'status' => $this->status?$this->status:"",
            'device_token' => $this->device_token?$this->device_token:"",
            'token' => $this->token,
            ];
            
    }
}
