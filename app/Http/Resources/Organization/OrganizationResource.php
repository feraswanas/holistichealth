<?php

namespace App\Http\Resources\Organization;

use Illuminate\Http\Resources\Json\JsonResource;

class OrganizationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return[
            'id' => $this->id,   
            'name' => $this->name?$this->name:"",   
            'status' => $this->status?$this->status:"",
            'image' => $this->image?$this->image:"",   
            'major' => $this->major?json_decode($this->major):[""],
            'email' => $this->email?$this->email:"",
            // 'rate' => $this->rate?$this->rate:"",
            'tax' => $this->tax?$this->tax:"",
            'phone1' => $this->phone1?$this->phone1:"",
            'phone2' => $this->phone2?$this->phone2:"",
            'address' => $this->address?$this->address:"",
            'lat' => $this->lat?$this->lat:"",
            'long' => $this->long?$this->long:"",
            'user_type' => $this->user_type?$this->user_type:"",
            'org_type' => $this->org_type?$this->org_type:"",

            'description' => $this->description?$this->description:"",
            'device_token' => $this->device_token?$this->device_token:"",
            'token' => $this->token,
            ];
    }
}
