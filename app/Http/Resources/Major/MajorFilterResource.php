<?php

namespace App\Http\Resources\Major;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Crypt;
use App\Http\Resources\Doctor\DoctorUpdateResource;

class MajorFilterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return[
            'id' => $this->id,   
            'doctor_id' => $this->doctor_id?$this->doctor_id:"",   
            'patient_id' => $this->patient_id?$this->patient_id:"",
            'description' => $this->description?Crypt::decryptString($this->description):"",   
            'prescription' => $this->prescription?json_decode(Crypt::decryptString($this->prescription)):"",
            'chronic' => $this->chronic?Crypt::decryptString($this->chronic):"",
            'priority' => $this->priority?$this->priority:"",
            'date' => $this->date?$this->date:"",
            'doctor' => $this->doctor?DoctorUpdateResource::make($this->doctor):"",

            // 'patient'=>$this->doctor?PatientUpdateResource::make($this->patient):""
            ];  
    }
}
