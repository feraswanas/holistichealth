<?php

namespace App\Http\Resources\Family;
use App\Http\Resources\Patient\PatientUpdateResource;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Resources\Json\JsonResource;

class RequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $id=Auth::id();
        if($id==$this->user_id1)
        
        {   
        return[
            
            'id' => $this->id,   
            'user_id1' => $this->user_id1?$this->user_id1:"",   
            'user_id2' => $this->user_id2?$this->user_id2:"",
            'status' => $this->status?$this->status:"",   
            'relation' => $this->relation?$this->relation:"",
            // 'patient1' => $this->patient1?PatientUpdateResource::make($this->patient1):"",
            'patient2' => $this->patient2?PatientUpdateResource::make($this->patient2):"",
            // 'doctor'=>$this->doctor?DoctorUpdateResource::make($this->doctor):""
            ]; 
        }
       else if($id==$this->user_id2)
       {
            return[
            'id' => $this->id,   
            'user_id1' => $this->user_id1?$this->user_id1:"",   
            'user_id2' => $this->user_id2?$this->user_id2:"",
            'status' => $this->status?$this->status:"",   
            'relation' => $this->relation?$this->relation:"",
            'patient2' => $this->patient1?PatientUpdateResource::make($this->patient1):"",
            // 'patient2' => $this->patient2?PatientUpdateResource::make($this->patient2):"",
            // 'doctor'=>$this->doctor?DoctorUpdateResource::make($this->doctor):""
            ]; 
       }

    
    }
}
