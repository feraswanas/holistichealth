<?php

namespace App\Http\Resources\Patient;

use Illuminate\Http\Resources\Json\JsonResource;

class PatientEmergencyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return[
            'id' => $this->id,   
            'name' => $this->name?$this->name:"",   
            'birth_date' => $this->birth_date?$this->birth_date:"",
            'gender' => $this->gender?$this->gender:"",   
            'cronic' => $this->cronic?json_decode($this->cronic):[],
            'email' => $this->email?$this->email:"",
            'blood_type' => $this->blood_type?$this->blood_type:"",
            'phone' => $this->phone?$this->phone:"",
            'id_number' => $this->id_number?$this->id_number:"",
            'picture' => $this->picture?$this->picture:"",
            'user_type' => $this->user_type?$this->user_type:"",
            'device_token' => $this->device_token?$this->device_token:"",
            'qr' => $this->qr?$this->qr:"",
            'qr_constant' => $this->qr_constant?$this->qr_constant:"",
            'user_type' => $this->user_type?$this->user_type:"",
            'emergency1'=>$this->emergency_1?PatientUpdateResource::make($this->emergency_1):"",
            'emergency2'=>$this->emergency_2?PatientUpdateResource::make($this->emergency_2):"",
            'emergency3'=>$this->emergency_3?PatientUpdateResource::make($this->emergency_3):""

        ];
    }
}
