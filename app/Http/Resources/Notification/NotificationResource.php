<?php

namespace App\Http\Resources\Notification;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return[
            'id' => $this->id,   
            'user_id' => $this->user_id?$this->user_id:"",   
            'seen' => $this->seen?$this->seen:"",
            'case' => $this->case?$this->case:0,
            'body' => $this->body?$this->body:"",
            'title' => $this->title?$this->title:"",
            'lat' => $this->lat?$this->lat:"",
            'longitude' => $this->longitude?$this->longitude:"",
            'image' => $this->image?$this->image:"",


            // 'patient'=>$this->doctor?PatientUpdateResource::make($this->patient):""
            ];  
    }
}
