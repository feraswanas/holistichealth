<?php

namespace App\Http\Resources\Operation;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Doctor\DoctorUpdateResource;
use App\Http\Resources\Organization\OrganizationUpdateResource;
use App\Http\Resources\Patient\PatientUpdateResource;

class OperationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return[
            'id' => $this->id,   
            'status' => $this->status?$this->status:"",   
            'date' => $this->date?$this->date:"",
            'description' => $this->description?$this->description:"",   
            'doctor1' => $this->doctor1?DoctorUpdateResource::make($this->doctor1):"",
            'doctor2' => $this->doctor2?DoctorUpdateResource::make($this->doctor2):"",
            'doctor3' => $this->doctor3?DoctorUpdateResource::make($this->doctor3):"",
            'org' => $this->organization?OrganizationUpdateResource::make($this->organization):"",
            'patient' => $this->patient?PatientUpdateResource::make($this->patient):""
      //
            ];
    }
}
