<?php

namespace App\Http\Resources\DoctorOrg;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\Doctor\DoctorUpdateResource;
use App\Http\Resources\Organization\OrganizationUpdateResource;

class DoctorOrgScheduleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        {
            return[
                'id' => $this->id,   
                'status' => $this->status?$this->status:"",   
                'from' => $this->from?$this->from:"",
                'gender' => $this->gender?$this->gender:"",   
                'doctor_id' => $this->doctor_id?$this->doctor_id:"",
                'org_id' => $this->org_id?$this->org_id:"",
                // 'doctor' => $this->doctor?DoctorUpdateResource::make($this->doctor):"",
                'org' => $this->organization?OrganizationUpdateResource::make($this->organization):"",
                // 'schedules' => $this->schedules?ScheduleResource::collection($this->schedules):"",
    
                ];
        }
    }
}
