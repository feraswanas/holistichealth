<?php

namespace App\Http\Resources\DoctorOrg;

use Illuminate\Http\Resources\Json\JsonResource;

class ScheduleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return[
            'id' => $this->id,   
            'doctor_org_id' => $this->doctor_org_id?$this->doctor_org_id:"",   
            'day' => $this->day?$this->day:"",
            'from' => $this->from?$this->from:"",   
            'to' => $this->to?$this->to:""
           
            ];

    }
}
