<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\History;
use Carbon\Carbon;
class everyday extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:everyday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return "succes";
         $date1 = Carbon::now()->subYear()->format('d/m/Y');
         $date2 = Carbon::now()->subYear(2)->format('d/m/Y');
         $date3 = Carbon::now()->subYear(3)->format('d/m/Y');
         History::where('date',$date1)->where('priority',1)->delete();
         History::where('date',$date2)->where('priority',2)->delete();
         History::where('date',$date3)->where('priority',3)->delete();

         return "succes";

        
    }
}
