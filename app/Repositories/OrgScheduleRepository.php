<?php

namespace App\Repositories;

use App\Models\OrgSchedule;
use App\Repositories\BaseRepository;

/**
 * Class OrgScheduleRepository
 * @package App\Repositories
 * @version May 10, 2022, 5:03 pm UTC
*/

class OrgScheduleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'doctor_org_id',
        'day',
        'from',
        'to'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrgSchedule::class;
    }
}
