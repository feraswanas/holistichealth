<?php

namespace App\Repositories;

use App\Models\Major;
use App\Repositories\BaseRepository;

/**
 * Class MajorRepository
 * @package App\Repositories
 * @version May 23, 2022, 3:15 pm UTC
*/

class MajorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'major'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Major::class;
    }
}
