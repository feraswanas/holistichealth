<?php

namespace App\Repositories;

use App\Models\DoctorOrg;
use App\Repositories\BaseRepository;

/**
 * Class DoctorOrgRepository
 * @package App\Repositories
 * @version April 27, 2022, 11:50 pm UTC
*/

class DoctorOrgRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'doctor_id',
        'org_id',
        'from',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DoctorOrg::class;
    }
}
