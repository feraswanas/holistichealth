<?php

namespace App\Repositories;

use App\Models\History;
use App\Repositories\BaseRepository;

/**
 * Class HistoryRepository
 * @package App\Repositories
 * @version March 1, 2022, 7:10 pm UTC
*/

class HistoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'doctor_id',
        'patient_id',
        'description',
        'prescription',
        'chronic',
        'priority',
        'date'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return History::class;
    }
}
