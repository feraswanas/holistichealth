<?php

namespace Database\Factories;

use App\Models\Operation;
use Illuminate\Database\Eloquent\Factories\Factory;

class OperationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Operation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'doctor1_id' => $this->faker->word,
        'doctor2_id' => $this->faker->word,
        'doctor3_id' => $this->faker->word,
        'patient_id' => $this->faker->word,
        'organization_id' => $this->faker->word,
        'description' => $this->faker->word,
        'status' => $this->faker->word,
        'date' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
