<?php

namespace Database\Factories;

use App\Models\Organization;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrganizationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Organization::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'status' => $this->faker->word,
        'name' => $this->faker->word,
        'image' => $this->faker->word,
        'email' => $this->faker->word,
        'password' => $this->faker->word,
        'phone1' => $this->faker->word,
        'phone2' => $this->faker->word,
        'location' => $this->faker->word,
        'tax' => $this->faker->word,
        'device_token' => $this->faker->word,
        'major' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
