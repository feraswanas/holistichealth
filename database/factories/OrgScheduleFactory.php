<?php

namespace Database\Factories;

use App\Models\OrgSchedule;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrgScheduleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OrgSchedule::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'doctor_org_id' => $this->faker->randomDigitNotNull,
        'day' => $this->faker->word,
        'from' => $this->faker->word,
        'to' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
