<?php

namespace Database\Factories;

use App\Models\Doctor;
use Illuminate\Database\Eloquent\Factories\Factory;

class DoctorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Doctor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'doctor' => $this->faker->word,
        'email' => $this->faker->word,
        'password' => $this->faker->word,
        'birth_date' => $this->faker->word,
        'gender' => $this->faker->word,
        'id_number' => $this->faker->word,
        'phone' => $this->faker->word,
        'major' => $this->faker->word,
        'certificate' => $this->faker->word,
        'device_token' => $this->faker->word,
        'user_type' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
