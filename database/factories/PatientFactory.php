<?php

namespace Database\Factories;

use App\Models\Patient;
use Illuminate\Database\Eloquent\Factories\Factory;

class PatientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Patient::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
        'email' => $this->faker->word,
        'phone' => $this->faker->word,
        'password' => $this->faker->word,
        'birth_date' => $this->faker->word,
        'gender' => $this->faker->word,
        'id_number' => $this->faker->word,
        'blood_type' => $this->faker->word,
        'cronic' => $this->faker->word,
        'qr' => $this->faker->word,
        'qr_constant' => $this->faker->word,
        'picture' => $this->faker->word,
        'user_type' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
