<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->string('password');
            $table->string('birth_date');
            $table->string('gender');
            $table->string('id_number');
            $table->string('blood_type');
            $table->string('cronic')->nullable();
            $table->string('qr')->nullable();
            $table->string('qr_constant');
            $table->string('device_token')->nullable();

            $table->string('picture')->nullable();
            $table->integer('user_type')->default(1);
            $table->timestamps();
            // $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('patients');
    }
}
