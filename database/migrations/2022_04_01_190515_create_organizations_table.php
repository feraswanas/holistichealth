<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganizationsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('status')->default('PENDING');
            $table->string('name');
            $table->string('image')->nullable();
            $table->string('email');
            $table->string('password');
            $table->string('phone1');
            $table->string('phone2')->nullable();
            $table->string('lat')->nullable();
            $table->string('long')->nullable();
            $table->string('address')->nullable();
            $table->string('tax');
            $table->string('device_token')->nullable();
            $table->string('major')->nullable();
            $table->string('description')->nullable();
            $table->integer('user_type')->default(3);
            $table->integer('org_type');


            $table->timestamps();
            // $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('organizations');
    }
}
