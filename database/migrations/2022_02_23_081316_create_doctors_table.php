<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('name');
            $table->string('email');
            $table->string('status')->default("PENDING");
            $table->string('password');
            $table->string('birth_date');
            $table->string('gender');
            $table->string('id_number');
            $table->string('phone');
            $table->string('major');
            $table->string('certificate');
            $table->string('description');
            $table->float('rate')->default(0);
            $table->string('picture')->nullable();
            $table->string('device_token')->nullable();
            $table->integer('user_type')->default(2);
            $table->timestamps();
            // $table->softDeletes();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('doctors');
    }
}
