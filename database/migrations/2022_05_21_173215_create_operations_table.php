<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOperationsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operations', function (Blueprint $table) {
            $table->id('id');
            $table->string('doctor1_id');
            $table->string('doctor2_id')->nullable();
            $table->string('doctor3_id')->nullable();
            $table->string('patient_id');
            $table->string('organization_id');
            $table->string('description');
            $table->string('status')->default('PENDING');
            $table->string('date');
            $table->timestamps();
            // $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('operations');
    }
}
