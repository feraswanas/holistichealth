<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('doctor/register', 'App\Http\Controllers\API\DoctorAPIController@store');
Route::post('doctor/login', 'App\Http\Controllers\API\DoctorAPIController@login');
// Route::resource('doctors', App\Http\Controllers\API\DoctorAPIController::class);
Route::post('patient/register', 'App\Http\Controllers\API\PatientAPIController@store');
Route::post('patient/login', 'App\Http\Controllers\API\PatientAPIController@login');
Route::get('organization/get_all', 'App\Http\Controllers\API\OrganizationAPIController@getAll');

Route::post('org/register', 'App\Http\Controllers\API\OrganizationAPIController@store');

Route::post('org/login', 'App\Http\Controllers\API\OrganizationAPIController@login');
Route::post('org/get_doctor', 'App\Http\Controllers\API\DoctorOrgAPIController@getDoctors');
Route::post('doctor/get_org', 'App\Http\Controllers\API\DoctorOrgAPIController@getOrganizations');
Route::post('org/get_doctor_reviews', 'App\Http\Controllers\API\DoctorAPIController@orgGetDoctorReviews');


Route::group(['middleware' => 'auth:patient-api'], function(){
    Route::post('patient/qr', 'App\Http\Controllers\API\PatientAPIController@qr');
    Route::post('patient/history', 'App\Http\Controllers\API\PatientAPIController@history');
    Route::get('patient/get_all', 'App\Http\Controllers\API\PatientAPIController@getAll');
    Route::post('patient/update', 'App\Http\Controllers\API\PatientAPIController@update');
    Route::post('family/get_request', 'App\Http\Controllers\API\FamilyAPIController@getRequest');
    Route::post('family/accept', 'App\Http\Controllers\API\FamilyAPIController@accept');
    Route::post('family/reject', 'App\Http\Controllers\API\FamilyAPIController@reject');
    Route::post('family/add', 'App\Http\Controllers\API\FamilyAPIController@store');
    Route::post('family/get_family', 'App\Http\Controllers\API\FamilyAPIController@getFamily');
    Route::get('doctor/top_rated', 'App\Http\Controllers\API\DoctorAPIController@topRated');
    Route::post('patient/details', 'App\Http\Controllers\API\PatientAPIController@details');
    Route::post('patient/add_emergency', 'App\Http\Controllers\API\PatientAPIController@addEmergency');
    Route::post('patient/get_emergency', 'App\Http\Controllers\API\PatientAPIController@getEmergency');
    Route::post('patient/emergency', 'App\Http\Controllers\API\PatientAPIController@emergency');
    Route::post('patient/check_password', 'App\Http\Controllers\API\PatientAPIController@checkPassword');
    Route::post('patient/remove_emergency', 'App\Http\Controllers\API\PatientAPIController@removeEmergency');

    Route::post('org/near_by', 'App\Http\Controllers\API\OrganizationAPIController@nearBy');

    // Route::resource('families', App\Http\Controllers\API\FamilyAPIController::class);


});
Route::group(['middleware' => 'auth:doctor-api'], function(){
    Route::post('patient/get/qr', 'App\Http\Controllers\API\PatientAPIController@getQr');
    Route::post('history/store', 'App\Http\Controllers\API\HistoryAPIController@store');
    Route::post('doctor/reviews/get', 'App\Http\Controllers\API\DoctorAPIController@getReviews');
    Route::post('doctor/details', 'App\Http\Controllers\API\DoctorAPIController@details');
    Route::post('doctor/history', 'App\Http\Controllers\API\DoctorAPIController@history');
    Route::post('doctor/update', 'App\Http\Controllers\API\DoctorAPIController@update');
    Route::post('doctor/get_pending_operation', 'App\Http\Controllers\API\DoctorAPIController@getPendingOperation');
    Route::post('operation/accept', 'App\Http\Controllers\API\OperationAPIController@accept');
    Route::post('operation/reject', 'App\Http\Controllers\API\OperationAPIController@reject');
    Route::post('major/filter_name', 'App\Http\Controllers\API\DoctorAPIController@filterMajor');
    Route::post('doctor/get_filtered_major', 'App\Http\Controllers\API\DoctorAPIController@getFilteredMajor');
    Route::post('doctor/check_password', 'App\Http\Controllers\API\DoctorAPIController@checkPassword');
    Route::post('doctor/check_schedule', 'App\Http\Controllers\API\OrgScheduleAPIController@checkSchedule');
    Route::post('schedule/add', 'App\Http\Controllers\API\OrgScheduleAPIController@store');





});


Route::group(['middleware' => 'auth:organization-api'], function(){
    Route::post('patient/get/qr_constant', 'App\Http\Controllers\API\PatientAPIController@getQrConstant');
    Route::post('organization/update', 'App\Http\Controllers\API\OrganizationAPIController@update');
    
    Route::post('organization/details', 'App\Http\Controllers\API\OrganizationAPIController@details');
    Route::get('doctor/get_all', 'App\Http\Controllers\API\DoctorAPIController@getAll');
    Route::post('operation/add', 'App\Http\Controllers\API\OperationAPIController@store');
    Route::post('organization/get_operations', 'App\Http\Controllers\API\OrganizationAPIController@getOperations');
    Route::post('organization/check_password', 'App\Http\Controllers\API\OrganizationAPIController@checkPassword');

    Route::post('patient/id_number', 'App\Http\Controllers\API\PatientAPIController@getIdNumber');


// Route::resource('operations', App\Http\Controllers\API\OperationAPIController::class);
    

});


Route::group(['middleware' => 'auth:organization-api,doctor-api'], function(){
    
    Route::resource('doctor_orgs', App\Http\Controllers\API\DoctorOrgAPIController::class);
    Route::post('doctor/get_pending', 'App\Http\Controllers\API\DoctorOrgAPIController@getPending');
    Route::post('doctor_org/accept', 'App\Http\Controllers\API\DoctorOrgAPIController@accept');
    Route::post('doctor_org/reject', 'App\Http\Controllers\API\DoctorOrgAPIController@reject');




});


Route::group(['middleware' => 'auth:organization-api,doctor-api,patient-api'], function(){
// Route::resource('notifications', App\Http\Controllers\API\NotificationAPIController::class);
Route::get('notification/get', 'App\Http\Controllers\API\NotificationAPIController@index');
Route::post('notification/seen', 'App\Http\Controllers\API\NotificationAPIController@seen');
Route::post('user/logout', 'App\Http\Controllers\API\PatientAPIController@logout');


});

// Route::resource('patients', App\Http\Controllers\API\PatientAPIController::class);


// Route::resource('histories', App\Http\Controllers\API\HistoryAPIController::class);


Route::resource('ratings', App\Http\Controllers\API\RatingAPIController::class);




Route::resource('organizations', App\Http\Controllers\API\OrganizationAPIController::class);




// Route::resource('org_schedules', App\Http\Controllers\API\OrgScheduleAPIController::class);
Route::post('schedule/delete', 'App\Http\Controllers\API\OrgScheduleAPIController@destroy');

Route::post('patient/get_operations', 'App\Http\Controllers\API\PatientAPIController@getOperations');

Route::post('schedule/get_doctor', 'App\Http\Controllers\API\OrgScheduleAPIController@getDoctor');

Route::post('schedule/get_doctor_without_org', 'App\Http\Controllers\API\OrgScheduleAPIController@getScheduleWithoutOrg');



// Route::resource('majors', App\Http\Controllers\API\MajorAPIController::class);
// Route::get('major/get', App\Http\Controllers\API\MajorAPIController::class);
Route::get('major/get', 'App\Http\Controllers\API\MajorAPIController@index');
Route::get('chronic/get', 'App\Http\Controllers\API\MajorAPIController@chronic');

Route::post('add/drug', 'App\Http\Controllers\API\MajorAPIController@addDrug');
Route::get('get/drug', 'App\Http\Controllers\API\MajorAPIController@getDrugs');

